package zisis.example.weatherdemo.mvp.presenter.weather

import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.validateMockitoUsage
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import zisis.example.weatherdemo.models.parsers.response.forecast.WeatherForecastResponse
import zisis.example.weatherdemo.mvp.interactor.weather.LocationWeatherForecastInteractorImplTest
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import zisis.example.weatherdemo.models.viewModel.DataResult
import zisis.example.weatherdemo.mvp.interactor.locationWeatherForecast.LocationWeatherForecastInteractor
import zisis.example.weatherdemo.mvp.presenter.locationWeatherForecast.LocationWeatherForecastPresenterImpl
import zisis.example.weatherdemo.mvp.view.locationWeatherForecast.LocationWeatherForecastView


class LocationWeatherForecastPresenterImplTest {

    private val gson = Gson()
    private val weatherDetailsResponse = gson.fromJson<WeatherForecastResponse>(
        LocationWeatherForecastInteractorImplTest::class.java.getResource("/location_weather_forecast_response.js")!!.readText(),
        WeatherForecastResponse::class.java
    )

    //Athens Greece weather forecast query
    private val weatherCode = 116
    private val query = "37.980,23.716"

    @ExperimentalCoroutinesApi
    private val testUiDispatcher = TestCoroutineDispatcher()
    @ExperimentalCoroutinesApi
    private val testBgDispatcher = TestCoroutineDispatcher()
    @ExperimentalCoroutinesApi
    private val testScope = TestCoroutineScope()

    @Mock
    lateinit var view: LocationWeatherForecastView
    @Mock
    lateinit var interactor: LocationWeatherForecastInteractor
    lateinit var presenter: LocationWeatherForecastPresenterImpl

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = LocationWeatherForecastPresenterImpl(view, interactor)
        presenter.updateDispatchersForTests(testUiDispatcher, testBgDispatcher, testScope)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testInit() = testScope.run {
        whenever(view.isAttached()).thenReturn(true)
        presenter.getWeatherForecast(query)
    }

    @Test
    fun getWeatherForecast() = runBlockingTest {
        whenever(view.isAttached()).thenReturn(true)
        whenever(interactor.getWeatherForecast(query)).thenReturn(
            DataResult(weatherDetailsResponse)
        )

        presenter.getWeatherForecast(query)
        verify(view, atLeastOnce()).isAttached()
        verify(view).showCurrentWeather(weatherDetailsResponse.response?.currentCondition?.get(0))
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        presenter.detach()
        verify(interactor).detach()
        testUiDispatcher.cancel()
        testBgDispatcher.cancel()
        validateMockitoUsage()
    }
}