package zisis.example.weatherdemo.mvp.presenter.base

import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import zisis.example.weatherdemo.mvp.interactor.base.BaseInteractor
import zisis.example.weatherdemo.mvp.interactor.base.MVPInteractor
import zisis.example.weatherdemo.mvp.view.base.MVPView

class BasePresenterTest {

    @Mock
    lateinit var view: MVPView

    lateinit var interactor: BaseInteractor

    lateinit var presenter: BasePresenter<MVPView, MVPInteractor>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        interactor = BaseInteractor()
        presenter = BasePresenter(view, interactor)
    }

    @Test
    fun detach() {
        whenever(view.isAttached()).thenReturn(true)

        assertNotNull(presenter.getView())
        assertNotNull(presenter.getInteractor())
        assert(presenter.isViewAttached())

        presenter.detach()
        interactor.detach()

        assertNull(presenter.getView())
        assertNull(presenter.getInteractor())
        assert(!presenter.isViewAttached())
    }

    @Test
    fun getView() {
        assertNotNull(presenter.getView())
    }

    @Test
    fun getInteractor() {
        assertNotNull(presenter.getInteractor())
    }

    @Test
    fun isViewAttached() {
        whenever(view.isAttached()).thenReturn(true)
        assert(presenter.isViewAttached())
        whenever(view.isAttached()).thenReturn(false)
        assert(!presenter.isViewAttached())
        whenever(view.isAttached()).thenReturn(true)
        presenter.detach()
        assert(!presenter.isViewAttached())
    }
}