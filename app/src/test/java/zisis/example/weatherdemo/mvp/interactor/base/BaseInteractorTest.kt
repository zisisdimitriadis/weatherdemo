package zisis.example.weatherdemo.mvp.interactor.base

import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BaseInteractorTest {

    @Mock
    lateinit var baseInteractor: BaseInteractor

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun detach() {
        baseInteractor.detach()
        verify(baseInteractor).detach()
    }

}