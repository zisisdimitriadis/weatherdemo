package zisis.example.weatherdemo.mvp.interactor.weather

import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import zisis.example.weatherdemo.models.parsers.response.forecast.WeatherForecastResponse
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.locationWeatherForecast.LocationWeatherForecastInteractor
import zisis.example.weatherdemo.mvp.interactor.locationWeatherForecast.LocationWeatherForecastInteractorImpl
import zisis.example.weatherdemo.persistent.database.WeatherDemoDatabase
import zisis.example.weatherdemo.persistent.database.dao.placeItem.PlaceItemDao
import zisis.example.weatherdemo.providers.network.NetworkProvider

class LocationWeatherForecastInteractorImplTest {

    private val gson = Gson()
    private val weatherDetailsResponse = gson.fromJson<WeatherForecastResponse>(
        LocationWeatherForecastInteractorImplTest::class.java.getResource("/location_weather_forecast_response.js")!!.readText(),
        WeatherForecastResponse::class.java
    )

    //Athens Greece weather forecast query
    private val weatherCode = 116
    private val query = "37.980,23.716"

    @Mock
    lateinit var locationWeatherForecastInteractor: LocationWeatherForecastInteractor
    @Mock
    lateinit var networkProvider: NetworkProvider
    @Mock
    lateinit var placeItemDao: PlaceItemDao
    private lateinit var locationWeatherForecastInteractorImpl: LocationWeatherForecastInteractorImpl

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        val weatherDemoDatabase: WeatherDemoDatabase = Mockito.mock(WeatherDemoDatabase::class.java, Mockito.CALLS_REAL_METHODS)
        Mockito
            .`when`(weatherDemoDatabase.placeItemTableDao())
            .thenReturn(placeItemDao)
        locationWeatherForecastInteractorImpl = LocationWeatherForecastInteractorImpl(networkProvider, weatherDemoDatabase)
    }

    @After
    fun tearDown() {
        locationWeatherForecastInteractorImpl.detach()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getLocationWeatherForecast() = runBlocking {
        whenever(networkProvider.getWeatherForeCastAsync(query)).thenReturn(
            CompletableDeferred(
                weatherDetailsResponse
            )
        )
        val response = locationWeatherForecastInteractorImpl.getWeatherForecast(query)
        Assert.assertNotNull(response)
        Assert.assertNotNull(response.data)
        Assert.assertEquals(weatherCode, response.data?.response?.currentCondition?.get(0)?.weatherCode?.toInt())
    }
}