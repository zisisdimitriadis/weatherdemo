package zisis.example.weatherdemo.providers.network.api

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import zisis.example.weatherdemo.common.DefinitionsApi
import zisis.example.weatherdemo.models.parsers.response.forecast.WeatherForecastResponse
import zisis.example.weatherdemo.models.parsers.response.search.SearchApiResponse
import zisis.example.weatherdemo.providers.network.NetworkProvider
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit

class WeatherClient(private val appContext: Context): NetworkProvider {

    private var weatherApi: WeatherApi
    private var applicationContext: WeakReference<Context>? = null

    init {
        applicationContext = WeakReference(appContext)
        weatherApi = createApi(appContext)
    }

    private fun createApi(appContext: Context): WeatherApi {
        return retrofit().create(WeatherApi::class.java)
    }

    private fun getOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val newUrl = chain.request().url()
                    .newBuilder()
                    .addQueryParameter("key", DefinitionsApi.key)
                    .addQueryParameter("format", DefinitionsApi.FORMAT)
                    .build()

                val newRequest = chain.request()
                    .newBuilder()
                    .url(newUrl)
                    .build()

                chain.proceed(newRequest)
            }
        return builder.build()
    }

    private fun retrofit(): Retrofit = Retrofit.Builder()
        .client(getOkHttpClient())
        .baseUrl(DefinitionsApi.DOMAIN)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory()).build()


   // Get weather forecast
    override fun getWeatherForeCastAsync(queryText: String): Deferred<WeatherForecastResponse> {
        return weatherApi.getWeatherForeCastAsync(queryText)
    }

    // Search Auto Complete
    override fun searchAutoCompleteAsync(queryText: String): Deferred<SearchApiResponse> {
        return weatherApi.searchAutoCompleteAsync(queryText)
    }
}