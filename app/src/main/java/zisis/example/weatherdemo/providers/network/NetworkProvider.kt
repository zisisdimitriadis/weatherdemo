package zisis.example.weatherdemo.providers.network

import kotlinx.coroutines.Deferred
import zisis.example.weatherdemo.models.parsers.response.forecast.WeatherForecastResponse
import zisis.example.weatherdemo.models.parsers.response.search.SearchApiResponse

interface NetworkProvider {

    fun getWeatherForeCastAsync(queryText: String): Deferred<WeatherForecastResponse>
    fun searchAutoCompleteAsync(queryText: String): Deferred<SearchApiResponse>

}