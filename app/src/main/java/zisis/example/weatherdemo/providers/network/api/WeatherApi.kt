package zisis.example.weatherdemo.providers.network.api

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query
import zisis.example.weatherdemo.common.DefinitionsApi
import zisis.example.weatherdemo.models.parsers.response.forecast.WeatherForecastResponse
import zisis.example.weatherdemo.models.parsers.response.search.SearchApiResponse

interface WeatherApi {

    @GET("weather.ashx/")
    fun getWeatherForeCastAsync(@Query("q") queryText: String, @Query("num_of_days") numOfDays: Int = DefinitionsApi.NUM_OF_DAYS): Deferred<WeatherForecastResponse>

    @GET("search.ashx/")
    fun searchAutoCompleteAsync(@Query("q") queryText: String, @Query("num_of_results") numOfResults: Int = DefinitionsApi.NUM_OF_RESULTS): Deferred<SearchApiResponse>
}