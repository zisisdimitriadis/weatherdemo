package zisis.example.weatherdemo.models.parsers.response.forecast

import com.google.gson.annotations.SerializedName

data class ForeCastRequest(
    @SerializedName("type") val type: String? = null,
    @SerializedName("query") val query: String? = null
) {
}