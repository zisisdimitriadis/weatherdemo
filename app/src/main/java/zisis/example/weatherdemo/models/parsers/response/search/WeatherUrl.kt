package zisis.example.weatherdemo.models.parsers.response.search

import com.google.gson.annotations.SerializedName

data class WeatherUrl(
    @SerializedName("value") val value: String? = null
)