package zisis.example.weatherdemo.models.parsers.response.forecast

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class WeatherDescription(
    @SerializedName("value") val value: String? = null
): Parcelable {
}