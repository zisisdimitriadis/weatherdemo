package zisis.example.weatherdemo.models.parsers.response.forecast

import com.google.gson.annotations.SerializedName

data class ForecastCurrentCondition(
    @SerializedName("observation_time") val observationTime: String? = null,
    @SerializedName("temp_C") val tempC: String? = null,
    @SerializedName("temp_F") val tempF: String? = null,
    @SerializedName("weatherCode") val weatherCode: String? = null,
    @SerializedName("weatherIconUrl") val weatherIcon: List<WeatherIcon>? = null,
    @SerializedName("weatherDesc") val weatherDescription: List<WeatherDescription>? = null,
    @SerializedName("windspeedMiles") val windspeedMiles: String? = null,
    @SerializedName("windspeedKmph") val windspeedKmph: String? = null,
    @SerializedName("winddirDegree") val winddirDegree: String? = null,
    @SerializedName("winddir16Point") val winddir16Point: String? = null,
    @SerializedName("precipMM") val precipMM: String? = null,
    @SerializedName("precipInches") val precipInches: String? = null,
    @SerializedName("humidity") val humidity: String? = null,
    @SerializedName("visibility") val visibility: String? = null,
    @SerializedName("visibilityMiles") val visibilityMiles: String? = null,
    @SerializedName("pressure") val pressure: String? = null,
    @SerializedName("pressureInches") val pressureInches: String? = null,
    @SerializedName("cloudcover") val cloudcover: String? = null,
    @SerializedName("FeelsLikeC") val FeelsLikeC: String? = null,
    @SerializedName("FeelsLikeF") val FeelsLikeF: String? = null,
    @SerializedName("uvIndex") val uvIndex: Int? = null

) {
}