package zisis.example.weatherdemo.models.parsers.response.search

import com.google.gson.annotations.SerializedName

data class SearchApiResponse(
    @SerializedName("search_api") val searchApiResponse: SearchResponse? = null
)