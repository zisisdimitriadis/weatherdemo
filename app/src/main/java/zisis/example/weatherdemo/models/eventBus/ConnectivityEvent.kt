package zisis.example.weatherdemo.models.eventBus

data class ConnectivityEvent(val hasNetwork: Boolean)