package zisis.example.weatherdemo.models.viewModel.place

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import zisis.example.weatherdemo.common.ViewModel

@Parcelize
data class PlaceItemModel(
    val id: Long? = null,
    val area: String? = null,
    val country: String? = null,
    val region: String? = null,
    val latitude: Double? = null,
    val longitude: Double? = null
): Parcelable, ViewModel {

    fun getDisplayTitle(): String {
        return "$area, $country $region"
    }

    fun getLatLngForQuery(): String {
        return latitude.toString() + "," + longitude.toString()
    }
}