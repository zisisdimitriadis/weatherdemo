package zisis.example.weatherdemo.models.parsers.response.forecast

import com.google.gson.annotations.SerializedName

data class WeatherForecastResponse(
    @SerializedName("data") val response: ForeCastData? = null
) {
}