package zisis.example.weatherdemo.models.viewModel

data class DataResult<out T>(
    val data: T? = null,
    val throwable: Throwable? = null
)