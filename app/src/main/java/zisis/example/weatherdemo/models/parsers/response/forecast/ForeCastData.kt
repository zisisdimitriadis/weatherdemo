package zisis.example.weatherdemo.models.parsers.response.forecast

import com.google.gson.annotations.SerializedName

data class ForeCastData(
    @SerializedName("request") val request: List<ForeCastRequest>? = null,
    @SerializedName("current_condition") val currentCondition: List<ForecastCurrentCondition>? = null,
    @SerializedName("weather") val weather: List<Weather>? = null
) {
}