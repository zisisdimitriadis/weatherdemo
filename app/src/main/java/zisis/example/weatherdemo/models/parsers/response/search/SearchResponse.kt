package zisis.example.weatherdemo.models.parsers.response.search

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("result") val searchResult: List<SearchItem>? = null
)