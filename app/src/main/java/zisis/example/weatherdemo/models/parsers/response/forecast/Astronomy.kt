package zisis.example.weatherdemo.models.parsers.response.forecast

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Astronomy(
    @SerializedName("sunrise") val sunrise: String? = null,
    @SerializedName("sunset") val sunset: String? = null,
    @SerializedName("moonrise") val moonrise: String? = null,
    @SerializedName("moonset") val moonset: String? = null,
    @SerializedName("moon_phase") val moon_phase: String? = null,
    @SerializedName("moon_illumination") val moon_illumination: String? = null

): Parcelable {
}