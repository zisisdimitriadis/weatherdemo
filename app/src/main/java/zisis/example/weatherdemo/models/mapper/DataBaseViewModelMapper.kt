package zisis.example.weatherdemo.models.mapper

import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.persistent.database.dao.placeItem.PlaceItemTable

fun toPlaceViewModel(placeItemTable: PlaceItemTable): PlaceItemModel {
    return PlaceItemModel(
        id = placeItemTable.id,
        area = placeItemTable.area,
        country = placeItemTable.country,
        region = placeItemTable.region,
        latitude = placeItemTable.latitude,
        longitude = placeItemTable.longitude
    )
}

fun toPlaceItemTable(placeItemModel: PlaceItemModel): PlaceItemTable {
    return PlaceItemTable(
        id = placeItemModel.id ?: 0,
        area = placeItemModel.area,
        country = placeItemModel.country,
        region = placeItemModel.region,
        latitude = placeItemModel.latitude,
        longitude = placeItemModel.longitude
    )
}