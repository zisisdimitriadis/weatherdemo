package zisis.example.weatherdemo.models.parsers.response.search

import com.google.gson.annotations.SerializedName

data class PlaceItem(
    @SerializedName("value") val value: String? = null
)