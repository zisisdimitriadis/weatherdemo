package zisis.example.weatherdemo.models.parsers.response.forecast

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Hourly(
    @SerializedName("time") val time: String? = null,
    @SerializedName("tempC") val tempC: String? = null,
    @SerializedName("tempF") val tempF: String? = null,
    @SerializedName("windspeedMiles") val windspeedMiles: String? = null,
    @SerializedName("windspeedKmph") val windspeedKmph: String? = null,
    @SerializedName("winddirDegree") val winddirDegree: String? = null,
    @SerializedName("winddir16Point") val winddir16Point: String? = null,
    @SerializedName("weatherCode") val weatherCode: String? = null,
    @SerializedName("weatherIconUrl") val weatherIconUrlList: List<WeatherIcon>? = null,
    @SerializedName("weatherDesc") val weatherDescList: List<WeatherDescription>? = null,
    @SerializedName("precipMM") val precipMM: String? = null,
    @SerializedName("precipInches") val precipInches: String? = null,
    @SerializedName("humidity") val humidity: String? = null,
    @SerializedName("visibility") val visibility: String? = null,
    @SerializedName("visibilityMiles") val visibilityMiles: String? = null,
    @SerializedName("pressure") val pressure: String? = null,
    @SerializedName("pressureInches") val pressureInches: String? = null,
    @SerializedName("cloudcover") val cloudcover: String? = null,
    @SerializedName("HeatIndexC") val heatIndexC: String? = null,
    @SerializedName("HeatIndexF") val heatIndexF: String? = null,
    @SerializedName("DewPointC") val dewPointC: String? = null,
    @SerializedName("DewPointF") val dewPointF: String? = null,
    @SerializedName("WindChillC") val windChillC: String? = null,
    @SerializedName("WindChillF") val windChillF: String? = null,
    @SerializedName("WindGustMiles") val windGustMiles: String? = null,
    @SerializedName("WindGustKmph") val windGustKmph: String? = null,
    @SerializedName("FeelsLikeC") val feelsLikeC: String? = null,
    @SerializedName("FeelsLikeF") val feelsLikeF: String? = null,
    @SerializedName("chanceofrain") val chanceofrain: String? = null,
    @SerializedName("chanceofremdry") val chanceofremdry: String? = null,
    @SerializedName("chanceofwindy") val chanceofwindy: String? = null,
    @SerializedName("chanceofovercast") val chanceofovercast: String? = null,
    @SerializedName("chanceofsunshine") val chanceofsunshine: String? = null,
    @SerializedName("chanceoffrost") val chanceoffrost: String? = null,
    @SerializedName("chanceofhightemp") val chanceofhightemp: String? = null,
    @SerializedName("chanceoffog") val chanceoffog: String? = null,
    @SerializedName("chanceofsnow") val chanceofsnow: String? = null,
    @SerializedName("chanceofthunder") val chanceofthunder: String? = null,
    @SerializedName("uvIndex") val uvIndex: String? = null


) : Parcelable {
}