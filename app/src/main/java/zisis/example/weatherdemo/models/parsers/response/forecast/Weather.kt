package zisis.example.weatherdemo.models.parsers.response.forecast

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Weather(
    @SerializedName("date") val date: String? = null,
    @SerializedName("astronomy") val astronomy: List<Astronomy>? = null,
    @SerializedName("maxtempC") val maxtempC: String? = null,
    @SerializedName("maxtempF") val maxtempF: String? = null,
    @SerializedName("mintempC") val mintempC: String? = null,
    @SerializedName("mintempF") val mintempF: String? = null,
    @SerializedName("avgtempC") val avgtempC: String? = null,
    @SerializedName("avgtempF") val avgtempF: String? = null,
    @SerializedName("totalSnow_cm") val totalSnow_cm: String? = null,
    @SerializedName("sunHour") val sunHour: String? = null,
    @SerializedName("uvIndex") val uvIndex: String? = null,
    @SerializedName("hourly") val hourlyList: List<Hourly>? = null

) : Parcelable {

    fun getDateInDayMonthYearFormat(): String {
        val separated = this.date?.split("-")
        if (separated != null) {
            return separated[2] + "-" + separated[1] + "-" + separated[0]
        } else {
            return this.date ?: ""
        }
    }
}