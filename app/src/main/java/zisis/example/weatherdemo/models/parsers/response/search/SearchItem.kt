package zisis.example.weatherdemo.models.parsers.response.search

import com.google.gson.annotations.SerializedName
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel

data class SearchItem(
    @SerializedName("areaName") val areaNameList: List<PlaceItem>? = null,
    @SerializedName("country") val countryList: List<PlaceItem>? = null,
    @SerializedName("region") val regionList: List<PlaceItem>? = null,
    @SerializedName("latitude") val latitude: String? = null,
    @SerializedName("longitude") val longitude: String? = null,
    @SerializedName("population") val population: String? = null,
    @SerializedName("weatherUrl") val weatherUrl: List<PlaceItem>? = null

) {

    fun getSearchItemTitle(): String {
        return areaNameList?.get(0)?.value + ", " +  countryList?.get(0)?.value + " " + regionList?.get(0)?.value
    }

    fun toPlaceItemModel(): PlaceItemModel {
        return PlaceItemModel(
            area = areaNameList?.get(0)?.value,
            country = countryList?.get(0)?.value,
            region = regionList?.get(0)?.value,
            latitude = this.latitude?.toDouble(),
            longitude = this.longitude?.toDouble()
        )
    }
}