package zisis.example.weatherdemo.common.deleagates

import android.app.Application
import timber.log.Timber
import zisis.example.weatherdemo.BuildConfig
import zisis.example.weatherdemo.common.deleagates.base.BaseDelegate

class ApplicationDelegate(
    application: Application
) : BaseDelegate<Application>(application) {

    fun initTimber() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

    }