package zisis.example.weatherdemo.common.extensions

import zisis.example.weatherdemo.common.extensions.DateFormats.DATE_FORMAT_WEATHER_DETAILS
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object DateFormats {
    const val DATE_FORMAT_WEATHER_DETAILS = "yyyy-MM-dd"
}


fun String.toWeekDay(): String? {
    return try {
        val formatter = SimpleDateFormat(DATE_FORMAT_WEATHER_DETAILS, Locale.getDefault())
        val date: Date
        date = formatter.parse(this)
        val dayFormatted: DateFormat = SimpleDateFormat("EEEE")
        dayFormatted.format(date)
    } catch (e: ParseException) {
        e.printStackTrace()
    } as String?
}
