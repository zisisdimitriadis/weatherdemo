package zisis.example.weatherdemo.common

object DefinitionsApi {
    const val DOMAIN = "https://api.worldweatheronline.com/premium/v1/"
    const val key = "03185ee205b2443caca124351202005"
    const val FORMAT = "json"

    const val NUM_OF_DAYS = 5
    const val NUM_OF_RESULTS = 10
}