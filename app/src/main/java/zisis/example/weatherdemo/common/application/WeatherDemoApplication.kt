package zisis.example.weatherdemo.common.application

import android.app.Application
import android.content.IntentFilter
import android.net.ConnectivityManager
import zisis.example.weatherdemo.common.deleagates.ApplicationDelegate
import zisis.example.weatherdemo.persistent.database.WeatherDemoDatabase
import zisis.example.weatherdemo.providers.network.NetworkProvider
import zisis.example.weatherdemo.providers.network.api.WeatherClient
import zisis.example.weatherdemo.receiver.connectivity.ConnectivityReceiver

class WeatherDemoApplication: Application() {
    companion object {
        private lateinit var instance: WeatherDemoApplication

        @JvmStatic
        fun get(): WeatherDemoApplication {
            return instance
        }
    }

    private val applicationDelegate = ApplicationDelegate(this)

    val weatherDemoDatabase: WeatherDemoDatabase by lazy {
        return@lazy WeatherDemoDatabase.get(this)
    }

    val networkProvider: NetworkProvider by lazy {
        return@lazy WeatherClient(this)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        applicationDelegate.initTimber()
    }

    fun registerConnectivityReceiverChange() {
        registerReceiver(ConnectivityReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }
}