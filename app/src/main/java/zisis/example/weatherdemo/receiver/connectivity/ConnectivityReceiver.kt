package zisis.example.weatherdemo.receiver.connectivity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import org.greenrobot.eventbus.EventBus
import zisis.example.weatherdemo.common.extensions.hasNetwork
import zisis.example.weatherdemo.models.eventBus.ConnectivityEvent

class ConnectivityReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val extras = intent.extras
        extras?.let {
            EventBus.getDefault().postSticky(ConnectivityEvent(context.hasNetwork()))
        }
    }
}