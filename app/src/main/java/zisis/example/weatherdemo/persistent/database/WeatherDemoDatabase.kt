package zisis.example.weatherdemo.persistent.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import zisis.example.weatherdemo.persistent.database.dao.placeItem.PlaceItemDao
import zisis.example.weatherdemo.persistent.database.dao.placeItem.PlaceItemTable

@Database(entities = [PlaceItemTable::class], version = 1, exportSchema = false)
abstract class WeatherDemoDatabase : RoomDatabase() {

    companion object {
        private const val DATABASE_NAME = "WeatherDemoDB"

        fun get(context: Context): WeatherDemoDatabase {
            return Room.databaseBuilder(context.applicationContext, WeatherDemoDatabase::class.java, DATABASE_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }
    }



    abstract fun placeItemTableDao(): PlaceItemDao
}