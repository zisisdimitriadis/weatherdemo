package zisis.example.weatherdemo.persistent.database.dao.placeItem

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "PlaceItemTable")
class PlaceItemTable(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    var area: String? = null,
    var country: String? = null,
    var region: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null
)