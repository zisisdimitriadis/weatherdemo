package zisis.example.weatherdemo.persistent.database.dao.placeItem

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PlaceItemDao {

    @Query("SELECT * FROM PlaceItemTable WHERE id = :placeId LIMIT 1")
    fun findPlace(placeId: Long): PlaceItemTable?
//
//    @Query("SELECT * FROM PlaceItemTable WHERE latitude = latitude and longitude = longitude LIMIT 1")
//    fun findPlace(latitude: Double, longitude: Double): PlaceItemTable?

    @Query("SELECT * FROM PlaceItemTable WHERE area = :areaName and country = :countryName and region = :regionName LIMIT 1")
    fun findPlace(areaName: String, countryName: String, regionName: String): PlaceItemTable?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(placeItemTable: PlaceItemTable)

    @Query("DELETE FROM PlaceItemTable WHERE id = :placeItemId ")
    fun deletePlace(placeItemId: Long)

    @Query("SELECT count(*) FROM PlaceItemTable")
    fun findAllPlaces(): Int

    @Query("SELECT * FROM PlaceItemTable")
    fun findAllPlacesAsList(): List<PlaceItemTable>?


}