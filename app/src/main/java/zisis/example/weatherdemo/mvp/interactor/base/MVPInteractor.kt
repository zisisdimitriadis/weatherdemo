package zisis.example.weatherdemo.mvp.interactor.base

interface MVPInteractor {
    fun detach()
}