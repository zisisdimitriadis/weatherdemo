package zisis.example.weatherdemo.mvp.interactor.search

import zisis.example.weatherdemo.models.parsers.response.search.SearchItem
import zisis.example.weatherdemo.models.viewModel.DataResult
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.base.MVPInteractor

interface SearchPlaceInteractor: MVPInteractor {
    suspend fun getSuggestions(text: String): DataResult<List<SearchItem>>

}