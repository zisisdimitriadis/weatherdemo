package zisis.example.weatherdemo.mvp.interactor.locationWeatherForecast

import zisis.example.weatherdemo.models.parsers.response.forecast.WeatherForecastResponse
import zisis.example.weatherdemo.models.viewModel.DataResult
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.base.MVPInteractor

interface LocationWeatherForecastInteractor : MVPInteractor {

    suspend fun getWeatherForecast(queryText: String): DataResult<WeatherForecastResponse>
    suspend fun addPlaceToFavorites(placeItemModel: PlaceItemModel): DataResult<Boolean>
    suspend fun isPlaceFavorite(placeItemModel: PlaceItemModel): DataResult<Boolean>
}