package zisis.example.weatherdemo.mvp.presenter.search

import zisis.example.weatherdemo.mvp.interactor.search.SearchPlaceInteractor
import zisis.example.weatherdemo.mvp.presenter.base.MVPPresenter
import zisis.example.weatherdemo.mvp.view.search.SearchPlaceView

interface SearchPlacePresenter: MVPPresenter<SearchPlaceView, SearchPlaceInteractor> {
    fun getSuggestions(query: String)
}