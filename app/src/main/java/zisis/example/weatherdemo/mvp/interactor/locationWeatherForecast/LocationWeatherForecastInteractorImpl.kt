package zisis.example.weatherdemo.mvp.interactor.locationWeatherForecast

import timber.log.Timber
import zisis.example.weatherdemo.models.mapper.toPlaceItemTable
import zisis.example.weatherdemo.models.parsers.response.forecast.WeatherForecastResponse
import zisis.example.weatherdemo.models.viewModel.DataResult
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.base.BaseInteractor
import zisis.example.weatherdemo.persistent.database.WeatherDemoDatabase
import zisis.example.weatherdemo.providers.network.NetworkProvider

class LocationWeatherForecastInteractorImpl(private val networkProvider: NetworkProvider, private val weatherDemoDatabase: WeatherDemoDatabase) :
    BaseInteractor(), LocationWeatherForecastInteractor {

    override suspend fun getWeatherForecast(queryText: String): DataResult<WeatherForecastResponse> {
        return try {
            val forecastResponse = networkProvider.getWeatherForeCastAsync(
                queryText
            ).await()
            return DataResult(
                forecastResponse
            )
        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }

    override suspend fun addPlaceToFavorites(placeItemModel: PlaceItemModel): DataResult<Boolean> {
        return try {
            val currentPlace = weatherDemoDatabase.placeItemTableDao()
                .findPlace(areaName = placeItemModel.area ?: "", countryName = placeItemModel.country ?: "", regionName = placeItemModel.region ?: "")
            if (currentPlace == null) {
                weatherDemoDatabase.placeItemTableDao()
                    .insert(placeItemTable = toPlaceItemTable(placeItemModel))
                DataResult(false)
            } else {
                DataResult(true)
            }

        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }

    override suspend fun isPlaceFavorite(placeItemModel: PlaceItemModel): DataResult<Boolean> {
        return try {
            val currentPlace = weatherDemoDatabase.placeItemTableDao()
                .findPlace(areaName = placeItemModel.area ?: "", countryName = placeItemModel.country ?: "", regionName = placeItemModel.region ?: "")
            DataResult(currentPlace != null)

        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }
}