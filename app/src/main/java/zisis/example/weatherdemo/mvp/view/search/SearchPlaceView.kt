package zisis.example.weatherdemo.mvp.view.search

import zisis.example.weatherdemo.models.parsers.response.search.SearchItem
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.view.base.MVPView

interface SearchPlaceView: MVPView {

    fun showSuggestions(placesList: List<SearchItem>)
}