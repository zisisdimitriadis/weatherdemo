package zisis.example.weatherdemo.mvp.presenter.base

import kotlinx.coroutines.*
import zisis.example.weatherdemo.mvp.interactor.base.MVPInteractor
import zisis.example.weatherdemo.mvp.view.base.MVPView
import java.lang.ref.WeakReference
import java.net.UnknownHostException

open class BasePresenter<out V : MVPView, out I : MVPInteractor>(view: V, interactor: I) :
    MVPPresenter<V, I>  {

    private var viewRef: WeakReference<V>? = null
    private var interactor: I? = interactor

    protected var job = SupervisorJob()
    protected var uiDispatcher: CoroutineDispatcher = Dispatchers.Main
    protected var bgDispatcher: CoroutineDispatcher = Dispatchers.IO
    protected var uiScope = CoroutineScope(Dispatchers.Main + job)

    init {
        viewRef = WeakReference(view)
    }

    override fun detach() {
        uiScope.coroutineContext.cancelChildren()
        viewRef?.clear()
        interactor?.detach()
        interactor = null
    }

    override fun getView(): V? {
        return viewRef?.get()
    }

    override fun getInteractor(): I? {
        return interactor
    }

    override fun isViewAttached(): Boolean {
        return viewRef != null && viewRef!!.get() != null && viewRef!!.get()!!.isAttached()
    }

    private fun isNetworkError(throwable: Throwable): Boolean {
        when (throwable) {
            is UnknownHostException -> {
                return true
            }
        }
        return false
    }

    fun onErrorThrowable(throwable: Throwable, isShowEmptyView: Boolean = false) {
        getView()?.hideLoader()
        if (isNetworkError(throwable)) {
            getView()?.showNoInternetError()
        } else {
            getView()?.showGenericError()
        }
        if (isShowEmptyView) {
            getView()?.showEmpty()
        }
    }

    fun updateDispatchersForTests(
        uiDispatcher: CoroutineDispatcher,
        bgDispatcher: CoroutineDispatcher,
        uiScope: CoroutineScope
    ) {
        this.uiDispatcher = uiDispatcher
        this.bgDispatcher = bgDispatcher
        this.uiScope = uiScope
    }
}