package zisis.example.weatherdemo.mvp.interactor.base

import com.google.gson.Gson
import retrofit2.HttpException
import timber.log.Timber

open class BaseInteractor: MVPInteractor {
    override fun detach() {}
}