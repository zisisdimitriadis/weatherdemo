package zisis.example.weatherdemo.mvp.presenter.locationWeatherForecast

import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.locationWeatherForecast.LocationWeatherForecastInteractor
import zisis.example.weatherdemo.mvp.presenter.base.MVPPresenter
import zisis.example.weatherdemo.mvp.view.locationWeatherForecast.LocationWeatherForecastView

interface LocationWeatherForecastPresenter :
    MVPPresenter<LocationWeatherForecastView, LocationWeatherForecastInteractor> {

    fun getWeatherForecast(queryText: String)
    fun setPlaceToFavorites(placeItemModel: PlaceItemModel)
    fun showFavoriteUIIfNeeded(placeItemModel: PlaceItemModel)

}