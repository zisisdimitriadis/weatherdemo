package zisis.example.weatherdemo.mvp.interactor.search

import timber.log.Timber
import zisis.example.weatherdemo.models.parsers.response.search.SearchItem
import zisis.example.weatherdemo.models.viewModel.DataResult
import zisis.example.weatherdemo.mvp.interactor.base.BaseInteractor
import zisis.example.weatherdemo.providers.network.NetworkProvider

class SearchPlaceInteractorImpl(private val networkProvider: NetworkProvider): BaseInteractor(), SearchPlaceInteractor {

    override suspend fun getSuggestions(text: String): DataResult<List<SearchItem>> {
        return try {
            val response = networkProvider.searchAutoCompleteAsync(text).await()
            DataResult(response.searchApiResponse?.searchResult)
        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }
}