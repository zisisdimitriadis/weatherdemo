package zisis.example.weatherdemo.mvp.view.home

import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.view.base.MVPView

interface HomeView: MVPView {

    fun showFavoritePlaces(favoritePlacesList: List<PlaceItemModel>)
    fun showEmptyFavoritePlaces()
    fun showFailDeleteMessage()
    fun updateNewFavoriteList()
}