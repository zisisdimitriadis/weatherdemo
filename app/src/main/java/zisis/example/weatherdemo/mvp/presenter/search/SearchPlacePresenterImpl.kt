package zisis.example.weatherdemo.mvp.presenter.search

import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import zisis.example.weatherdemo.mvp.interactor.search.SearchPlaceInteractor
import zisis.example.weatherdemo.mvp.presenter.base.BasePresenter
import zisis.example.weatherdemo.mvp.view.search.SearchPlaceView

class SearchPlacePresenterImpl(
    view: SearchPlaceView,
    interactor: SearchPlaceInteractor
) : BasePresenter<SearchPlaceView, SearchPlaceInteractor>(view, interactor), SearchPlacePresenter {

    companion object {
        private const val MIN_QUERY_LENGTH = 3
    }

    override fun getSuggestions(query: String) {
        if (!isViewAttached() || query.isEmpty() || query.trim().length < MIN_QUERY_LENGTH) {
            return
        }
        getView()?.showLoader()
        uiScope.launch {
            val response = withContext(bgDispatcher) { getInteractor()?.getSuggestions(query) }
            if (!isViewAttached()) {
                return@launch
            }
            response?.data?.let { searchItemList ->
                getView()?.hideLoader()
                if (searchItemList.isNullOrEmpty()) {
                    getView()?.showEmpty()
                } else {
                    getView()?.showSuggestions(searchItemList)
                }
            }  ?: response?.throwable?.let {
                onErrorThrowable(it, false)
            } ?: run {
                getView()?.showGenericError()
                getView()?.hideLoader()
            }
        }
    }
}