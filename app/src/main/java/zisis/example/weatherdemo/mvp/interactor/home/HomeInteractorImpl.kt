package zisis.example.weatherdemo.mvp.interactor.home

import timber.log.Timber
import zisis.example.weatherdemo.models.mapper.toPlaceViewModel
import zisis.example.weatherdemo.models.viewModel.DataResult
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.base.BaseInteractor
import zisis.example.weatherdemo.persistent.database.WeatherDemoDatabase
import zisis.example.weatherdemo.persistent.database.dao.placeItem.PlaceItemTable

class HomeInteractorImpl(private val weatherDemoDatabase: WeatherDemoDatabase): BaseInteractor(), HomeInteractor {

    override suspend fun getFavoritePlaces(): DataResult<List<PlaceItemModel>?> {
        return try {
           val favoritePlacesList = weatherDemoDatabase.placeItemTableDao().findAllPlacesAsList()?.map {
               toPlaceViewModel(it)
           } ?: arrayListOf()
            DataResult(favoritePlacesList)
        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }

    override suspend fun removeFavoritePlace(placeId: Long): DataResult<Boolean> {
        return try {
            weatherDemoDatabase.placeItemTableDao()
                .deletePlace(placeItemId =  placeId)
            val hasPlaceDeleted = weatherDemoDatabase.placeItemTableDao().findPlace(placeId = placeId)
            DataResult(hasPlaceDeleted == null)
        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }


}