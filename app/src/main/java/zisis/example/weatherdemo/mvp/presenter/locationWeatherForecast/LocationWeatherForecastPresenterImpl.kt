package zisis.example.weatherdemo.mvp.presenter.locationWeatherForecast

import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.locationWeatherForecast.LocationWeatherForecastInteractor
import zisis.example.weatherdemo.mvp.presenter.base.BasePresenter
import zisis.example.weatherdemo.mvp.view.locationWeatherForecast.LocationWeatherForecastView

class LocationWeatherForecastPresenterImpl(
    view: LocationWeatherForecastView,
    interactor: LocationWeatherForecastInteractor
) : BasePresenter<LocationWeatherForecastView, LocationWeatherForecastInteractor>(view, interactor),
    LocationWeatherForecastPresenter {

    override fun getWeatherForecast(queryText: String) {
        if (!isViewAttached()) {
            return
        }
        uiScope.launch {
            getView()?.showLoader()

            val response = withContext(bgDispatcher) {
                getInteractor()?.getWeatherForecast(queryText)
            }
            response?.data?.let { weatherForecastResponse ->
                if (!isViewAttached()) {
                    return@launch
                }
                if (weatherForecastResponse.response == null || weatherForecastResponse.response.weather.isNullOrEmpty()) {
                    getView()?.showEmpty()
                } else {
                    val currentWeatherResponse = weatherForecastResponse.response.currentCondition
                    if (currentWeatherResponse.isNullOrEmpty()) {
                        getView()?.showEmptyCurrentWeather()
                    } else {
                        getView()?.showCurrentWeather(currentWeatherResponse[0])
                    }
                    val weatherResponse = weatherForecastResponse.response.weather
                    if (weatherResponse.isNullOrEmpty()) {
                        getView()?.showEmptyForecast()
                    } else {
                        getView()?.showForecast(weatherResponse)
                    }

                }
            } ?: response?.throwable?.let {
                onErrorThrowable(it, false)
            } ?: kotlin.run {
                getView()?.showGenericError()
                getView()?.showEmpty()
            }
            getView()?.hideLoader()
        }
    }

    override fun setPlaceToFavorites(placeItemModel: PlaceItemModel) {
        if (!isViewAttached()) {
            return
        }
        uiScope.launch {
            val response = withContext(bgDispatcher) {
                getInteractor()?.addPlaceToFavorites(placeItemModel)
            }
            if (!isViewAttached()) {
                return@launch
            }
            response?.data?.let { isPlaceFavorite ->
                if (isPlaceFavorite) {
                    return@launch
                }
                getView()?.updateFavoriteUI(true)
            } ?: kotlin.run {
                getView()?.showGenericError()
                getView()?.showEmpty() }

        }
    }

    override fun showFavoriteUIIfNeeded(placeItemModel: PlaceItemModel) {
        if (!isViewAttached()) {
            return
        }

        uiScope.launch {
            val response = withContext(bgDispatcher) {
                getInteractor()?.isPlaceFavorite(placeItemModel)
            }
            if (!isViewAttached()) {
                return@launch
            }
            response?.data?.let { isPlaceFavorite ->
                getView()?.updateFavoriteUI(isPlaceFavorite)
            } ?: kotlin.run {
                getView()?.showGenericError()
                getView()?.showEmpty() }

        }

    }
}