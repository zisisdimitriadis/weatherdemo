package zisis.example.weatherdemo.mvp.view.locationWeatherForecast

import zisis.example.weatherdemo.models.parsers.response.forecast.ForecastCurrentCondition
import zisis.example.weatherdemo.models.parsers.response.forecast.Weather
import zisis.example.weatherdemo.mvp.view.base.MVPView

interface LocationWeatherForecastView: MVPView {

    fun showEmptyCurrentWeather()
    fun showCurrentWeather(forecastCurrentCondition: ForecastCurrentCondition?)
    fun showEmptyForecast()
    fun showForecast(weatherModelList: List<Weather>)
    fun updateFavoriteUI(isFavorite: Boolean)
}