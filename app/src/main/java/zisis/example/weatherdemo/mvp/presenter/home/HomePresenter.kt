package zisis.example.weatherdemo.mvp.presenter.home

import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.home.HomeInteractor
import zisis.example.weatherdemo.mvp.presenter.base.MVPPresenter
import zisis.example.weatherdemo.mvp.view.home.HomeView

interface HomePresenter : MVPPresenter<HomeView, HomeInteractor> {
    fun getFavoritePlaces()
    fun removePlaceFromFavorites(placeItemModel: PlaceItemModel)
}