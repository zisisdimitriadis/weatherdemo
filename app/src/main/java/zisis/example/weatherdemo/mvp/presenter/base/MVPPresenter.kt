package zisis.example.weatherdemo.mvp.presenter.base

import zisis.example.weatherdemo.mvp.interactor.base.MVPInteractor
import zisis.example.weatherdemo.mvp.view.base.MVPView

interface MVPPresenter<out V: MVPView, out I: MVPInteractor> {
    fun detach()
    fun getView(): V?
    fun getInteractor(): I?
    fun isViewAttached() : Boolean
}