package zisis.example.weatherdemo.mvp.presenter.home

import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.home.HomeInteractor
import zisis.example.weatherdemo.mvp.presenter.base.BasePresenter
import zisis.example.weatherdemo.mvp.view.home.HomeView

class HomePresenterImpl(
    view: HomeView,
    interactor: HomeInteractor
) : BasePresenter<HomeView, HomeInteractor>(view, interactor), HomePresenter {
    override fun getFavoritePlaces() {
        if (!isViewAttached()) {
            return
        }
        uiScope.launch {
            val response = withContext(bgDispatcher) {
                getInteractor()?.getFavoritePlaces()
            }
            if (!isViewAttached()) {
                return@launch
            }
            response?.data?.let {
                if (it.isEmpty()) {
                    getView()?.showEmptyFavoritePlaces()
                    return@launch
                }
                getView()?.showFavoritePlaces(it)
            } ?: kotlin.run {
                getView()?.showGenericError()
                getView()?.showEmpty() }
        }

    }

    override fun removePlaceFromFavorites(placeItemModel: PlaceItemModel) {
        if (!isViewAttached()) {
            return
        }

        uiScope.launch {
            val response = withContext(bgDispatcher) {
                getInteractor()?.removeFavoritePlace(placeItemModel.id ?: -1)
            }
            if (!isViewAttached()) {
                return@launch
            }
            response?.data?.let { hasPlaceDeleted ->
                if (hasPlaceDeleted) {
                    getView()?.updateNewFavoriteList()
                    return@launch
                }
                getView()?.showFailDeleteMessage()
            } ?: kotlin.run {
                getView()?.showGenericError()
                getView()?.showEmpty() }
        }
    }

}