package zisis.example.weatherdemo.mvp.interactor.home

import zisis.example.weatherdemo.models.viewModel.DataResult
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.base.MVPInteractor
import zisis.example.weatherdemo.persistent.database.dao.placeItem.PlaceItemTable

interface HomeInteractor: MVPInteractor {

    suspend fun getFavoritePlaces(): DataResult<List<PlaceItemModel>?>
    suspend fun removeFavoritePlace(placeId: Long): DataResult<Boolean>
}