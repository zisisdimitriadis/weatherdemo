package zisis.example.weatherdemo.ui.activity.base

import android.view.View
import kotlinx.android.synthetic.main.layout_empty.*
import kotlinx.android.synthetic.main.layout_loading.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.mvp.interactor.base.MVPInteractor
import zisis.example.weatherdemo.mvp.presenter.base.MVPPresenter
import zisis.example.weatherdemo.mvp.view.base.MVPView

open class BaseMVPActivity<T : MVPPresenter<MVPView, MVPInteractor>> : BaseActivity(), MVPView {

    protected var presenter: T? = null

    override fun isAttached(): Boolean {
        return !isFinishing
    }

    override fun showLoader() {
        hideEmpty()
        loadingView?.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        loadingView?.visibility = View.GONE
    }

    override fun showEmpty() {
        hideLoader()
        emptyTextView?.text = getString(R.string.empty_data_text)
        emptyView?.visibility = View.VISIBLE
    }

    override fun hideEmpty() {
        emptyView?.visibility = View.GONE
    }

    override fun showError(error: String) {

    }

    override fun showNoInternetError() {
        showDialog(
            getString(R.string.dialog_error_title),
            getString(R.string.no_internet),
            getString(R.string.dialog_error_button)
        )
    }

    override fun showGenericError() {
        showDialog(
            getString(R.string.dialog_error_title),
            getString(R.string.dialog_wrong_search_text),
            getString(R.string.dialog_error_button)
        )
    }
}