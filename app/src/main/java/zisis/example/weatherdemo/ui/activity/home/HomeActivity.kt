package zisis.example.weatherdemo.ui.activity.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.layout_empty.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.common.application.WeatherDemoApplication
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.home.HomeInteractorImpl
import zisis.example.weatherdemo.mvp.presenter.home.HomePresenter
import zisis.example.weatherdemo.mvp.presenter.home.HomePresenterImpl
import zisis.example.weatherdemo.mvp.view.home.HomeView
import zisis.example.weatherdemo.ui.activity.base.BaseActivity
import zisis.example.weatherdemo.ui.activity.base.BaseMVPActivity
import zisis.example.weatherdemo.ui.activity.locationForecast.LocationWeatherForecastActivity
import zisis.example.weatherdemo.ui.activity.search.SearchPlaceActivity
import zisis.example.weatherdemo.ui.adapter.home.FavoritePlacesRecyclerViewAdapter

class HomeActivity: BaseMVPActivity<HomePresenter>(), HomeView {

    companion object {
        const val PLACE_ITEM_MODEL = "PlaceItemModel"
    }

    private lateinit var favoritePlacesRecyclerViewAdapter: FavoritePlacesRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        presenter = HomePresenterImpl(this, HomeInteractorImpl(WeatherDemoApplication.get().weatherDemoDatabase))
        initLayout()
        presenter?.getFavoritePlaces()
    }

    override fun onResume() {
        super.onResume()
        presenter?.getFavoritePlaces()
    }

    private fun initLayout() {
        toolbarTextView?.text = "Αγαπημένες τοποθεσίες"
        backButtonImageView?.visibility = View.GONE
        favoritePlacesRecyclerView?.layoutManager = LinearLayoutManager(this)
        favoritePlacesRecyclerViewAdapter = FavoritePlacesRecyclerViewAdapter(
            onAddNewPlaceClicked = {
                startActivityWithTransition(Intent(this, SearchPlaceActivity::class.java))
            },
            onDeletePlaceClicked = { placeItemModel ->
                showDialog(titleText = getString(R.string.dialog_delete_favorite_place_title),
                    messageText = getString(R.string.dialog_delete_favorite_place_text),
                    leftButtonText = getString(R.string.dialog_no_button),
                    rightButtonText = getString(R.string.dialog_yes_button),
                    leftButtonListener = {
                        it.dismiss()
                    },
                    rightButtonListener = {
                        presenter?.removePlaceFromFavorites(placeItemModel)
                    }
                )

            },
            onPlaceClicked = { placeItemModel ->
                val locationWeatherForecastIntent = Intent(this, LocationWeatherForecastActivity::class.java)
                locationWeatherForecastIntent.putExtra(PLACE_ITEM_MODEL, placeItemModel)
                startActivityWithTransition(locationWeatherForecastIntent)
            }
        )
        favoritePlacesRecyclerView?.adapter = favoritePlacesRecyclerViewAdapter
        val searchPlaceIntent =  Intent(this, SearchPlaceActivity::class.java)
        addLocationButton.setOnClickListener {
            startActivityWithTransition(searchPlaceIntent)
        }
    }

    override fun showFavoritePlaces(favoritePlacesList: List<PlaceItemModel>) {
        favoritePlacesRecyclerViewAdapter.setPlaceList(favoritePlacesList)
        emptyFavoritePlacesView?.visibility = View.GONE
        favoritePlacesRecyclerView?.visibility = View.VISIBLE
    }

    override fun showEmptyFavoritePlaces() {
        emptyFavoritePlacesView?.visibility = View.VISIBLE
        favoritePlacesRecyclerView?.visibility = View.GONE
    }

    override fun showFailDeleteMessage() {
        showDialog(
            titleText = getString(R.string.dialog_favorite_delete_failed),
            rightButtonText = getString(R.string.dialog_error_ok),
            rightButtonListener = {
                it.dismiss()
            }
        )
    }

    override fun updateNewFavoriteList() {
        presenter?.getFavoritePlaces()
    }


}