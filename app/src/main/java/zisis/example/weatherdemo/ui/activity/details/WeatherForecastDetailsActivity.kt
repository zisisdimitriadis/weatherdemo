package zisis.example.weatherdemo.ui.activity.details

import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_weather_details.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.common.extensions.toWeekDay
import zisis.example.weatherdemo.models.parsers.response.forecast.Weather
import zisis.example.weatherdemo.ui.activity.base.BaseActivity
import zisis.example.weatherdemo.ui.adapter.forecast.ForecastRecyclerViewAdapter
import zisis.example.weatherdemo.ui.adapter.forecast.hourly.HourlyForecastRecyclerViewAdapter

class WeatherForecastDetailsActivity : BaseActivity() {

    companion object {
        const val WEATHER_INFO = "WeatherInfo"
        const val POSITION = "position"
    }

    private lateinit var weather: Weather
    private var position: Int = 0
    private lateinit var hourlyForecastRecyclerViewAdapter: HourlyForecastRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_details)
        getPassData()
        initLayout()
    }

    private fun getPassData() {
        weather = intent?.extras?.getParcelable(WEATHER_INFO) ?: Weather()
        position = intent?.extras?.getInt(POSITION) ?: 0
    }

    private fun initLayout() {
        toolbarTextView?.text = getString(R.string.details_toolbar_text)
        dateTextView?.text = weather.getDateInDayMonthYearFormat()
        weekDayTextView?.text = weather.date?.toWeekDay()
        cardView?.setCardBackgroundColor(ContextCompat.getColor(this, getColorByPosition(position)))
        averageTempTextView?.text = weather.avgtempC
        minTempTextView?.text = weather.mintempC
        maxTempTextView?.text = weather.maxtempC
        backButtonImageView.visibility = View.GONE

        closeFabButton?.setOnClickListener {
            finish()
            overridePendingTransition(0, R.anim.animation_slide_down)
            //ActivityCompat.finishAfterTransition(this);
        }
        hourlyRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        hourlyForecastRecyclerViewAdapter = HourlyForecastRecyclerViewAdapter()
        weather.hourlyList?.let { hourlyForecastRecyclerViewAdapter.setHourlyList(it) }
        hourlyRecyclerView?.adapter = hourlyForecastRecyclerViewAdapter

    }

    private fun getColorByPosition(position: Int): Int {
        return when (position) {
            0 -> R.color.color_rose
            1 -> R.color.color_yellow_sea
            2 -> R.color.color_azure_radiance
            3 -> R.color.color_monza
            4 -> R.color.color_blue_ribbon
            else -> R.color.color_persian_blue
        }
    }

}