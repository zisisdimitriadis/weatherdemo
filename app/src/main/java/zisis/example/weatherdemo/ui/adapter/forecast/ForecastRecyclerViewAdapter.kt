package zisis.example.weatherdemo.ui.adapter.forecast

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_forecast_item.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.common.extensions.toWeekDay
import zisis.example.weatherdemo.models.parsers.response.forecast.Weather

class ForecastRecyclerViewAdapter(private val onWeatherForecastClicked: (weather: Weather, position: Int) -> Unit) :
    RecyclerView.Adapter<ForecastRecyclerViewAdapter.WeatherForecastItemViewHolder>() {

    private var weatherList = mutableListOf<Weather>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WeatherForecastItemViewHolder {
        return WeatherForecastItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_forecast_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return weatherList.size
    }

    fun setWeatherList(weatherList: List<Weather>) {
        this.weatherList.clear()
        this.weatherList = weatherList.toMutableList()
        notifyDataSetChanged()

    }

    override fun onBindViewHolder(holder: WeatherForecastItemViewHolder, position: Int) {
        val weatherModel = weatherList[position]
        holder.bind(weatherModel, position, onWeatherForecastClicked)
    }

    class WeatherForecastItemViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        fun bind(weather: Weather, position: Int, onWeatherForecastClicked: (weather: Weather, position: Int) -> Unit) {
            dateTextView?.text = weather.getDateInDayMonthYearFormat()
            weekDayTextView?.text = weather.date?.toWeekDay()
            averageTempTextView?.text = weather.avgtempC
            minTempTextView?.text = weather.mintempC
            maxTempTextView?.text = weather.maxtempC
            mainView?.setCardBackgroundColor(ContextCompat.getColor(containerView.context, getColorByPosition(position)))
            itemView.setOnClickListener {
                onWeatherForecastClicked(weather, position)
            }
        }
    }
}

fun getColorByPosition(position: Int): Int {
    return when(position) {
        0 -> R.color.color_rose
        1 -> R.color.color_yellow_sea
        2 -> R.color.color_azure_radiance
        3 -> R.color.color_monza
        4 -> R.color.color_blue_ribbon
        else -> R.color.color_persian_blue
    }
}
