package zisis.example.weatherdemo.ui.adapter.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daimajia.swipe.SwipeLayout
import com.malinskiy.superrecyclerview.swipe.BaseSwipeAdapter
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_favorite_place.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel


class FavoritePlacesRecyclerViewAdapter(
    private val onPlaceClicked: (PlaceItemModel) -> Unit,
    private var onAddNewPlaceClicked: () -> Unit,
    private var onDeletePlaceClicked: (PlaceItemModel) -> Unit
) :
    BaseSwipeAdapter<BaseSwipeAdapter.BaseSwipeableViewHolder>() {

    private var favoritePlacesList = mutableListOf<PlaceItemModel>()

    companion object {
        private const val TYPE_PLACE = 0
        private const val TYPE_FOOTER = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseSwipeableViewHolder {
        return when (viewType) {
            TYPE_PLACE -> PlaceViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.row_favorite_place,
                    parent,
                    false
                )
            )

            else -> PlaceFooterViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.row_favorite_place_add_new,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: BaseSwipeableViewHolder, position: Int) {
        if (holder is PlaceViewHolder) {
            val placeItemModel = favoritePlacesList[position]
            holder.bind(placeItemModel, onPlaceClicked)
            holder.deleteImageView.setOnClickListener {
                onDeletePlaceClicked(placeItemModel)
            }
        } else {
            val footerHolder = holder as PlaceFooterViewHolder
            footerHolder.bind(onAddNewPlaceClicked)
        }
    }

    override fun getItemCount(): Int {
        return favoritePlacesList.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            itemCount - 1 -> TYPE_FOOTER
            else -> TYPE_PLACE
        }
    }

    fun setPlaceList(placeList: List<PlaceItemModel>) {
        this.favoritePlacesList.clear()
        this.favoritePlacesList = placeList.toMutableList()
        notifyDataSetChanged()
    }

    class PlaceViewHolder(override val containerView: View) :
        LayoutContainer,
        BaseSwipeAdapter.BaseSwipeableViewHolder(containerView) {

        fun bind(placeItemModel: PlaceItemModel, onPlaceClicked: (PlaceItemModel) -> Unit) {
            favoritePlaceTitleTextView?.text = placeItemModel.getDisplayTitle()
            placeRelativeLayout?.setOnClickListener {
                onPlaceClicked(placeItemModel)
            }
        }

    }


    class PlaceFooterViewHolder(override val containerView: View) :
        LayoutContainer,
        BaseSwipeAdapter.BaseSwipeableViewHolder(containerView) {

        fun bind(onAddNewPlaceClicked: () -> Unit) {
            itemView.setOnClickListener {
                onAddNewPlaceClicked()
            }
        }

    }
}
