package zisis.example.weatherdemo.ui.adapter.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_search_place.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.models.parsers.response.search.SearchItem

class SearchPlaceRecyclerViewAdapter(private val onSearchItemClicked: (searchItem: SearchItem) -> Unit) :
    RecyclerView.Adapter<SearchPlaceRecyclerViewAdapter.SearchItemViewHolder>() {

    private var suggestionsList = mutableListOf<SearchItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchItemViewHolder {
        return SearchItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_search_place,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return suggestionsList.size
    }

    override fun onBindViewHolder(holder: SearchItemViewHolder, position: Int) {
        val searchItem = suggestionsList[position]
        holder.bind(searchItem, onSearchItemClicked)
    }

    fun setList(suggestionsList: List<SearchItem>) {
        this.suggestionsList.clear()
        this.suggestionsList = suggestionsList.toMutableList()
        notifyDataSetChanged()
    }

    class SearchItemViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        fun bind(searchItem: SearchItem, onSearchItemClicked: (SearchItem) -> Unit) {
            placeTextView?.text = searchItem.getSearchItemTitle()
            itemView?.setOnClickListener {
                onSearchItemClicked(searchItem)
            }
        }
    }
}