package zisis.example.weatherdemo.ui.activity.search

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_search_place.*
import kotlinx.android.synthetic.main.layout_no_internet_view.*
import kotlinx.android.synthetic.main.layout_search_place.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.common.application.WeatherDemoApplication
import zisis.example.weatherdemo.common.extensions.afterTextChangedDebounce
import zisis.example.weatherdemo.common.extensions.closeSoftKeyboard
import zisis.example.weatherdemo.models.parsers.response.search.SearchItem
import zisis.example.weatherdemo.mvp.interactor.search.SearchPlaceInteractorImpl
import zisis.example.weatherdemo.mvp.presenter.search.SearchPlacePresenter
import zisis.example.weatherdemo.mvp.presenter.search.SearchPlacePresenterImpl
import zisis.example.weatherdemo.mvp.view.search.SearchPlaceView
import zisis.example.weatherdemo.ui.activity.base.BaseActivity
import zisis.example.weatherdemo.ui.activity.base.BaseMVPActivity
import zisis.example.weatherdemo.ui.activity.locationForecast.LocationWeatherForecastActivity
import zisis.example.weatherdemo.ui.adapter.search.SearchPlaceRecyclerViewAdapter

class SearchPlaceActivity : BaseMVPActivity<SearchPlacePresenter>(), SearchPlaceView {

    companion object {
        const val DELAY_MILLIS: Long = 400L
        const val PLACE_ITEM_MODEL = "PlaceItemModel"
    }

    private lateinit var searchPlaceRecyclerViewAdapter: SearchPlaceRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_place)
        presenter = SearchPlacePresenterImpl(
            this,
            SearchPlaceInteractorImpl(WeatherDemoApplication.get().networkProvider)
        )
        initLayout()
    }

    private fun initLayout() {
        searchPlaceRecyclerView?.layoutManager = LinearLayoutManager(this)
        searchPlaceRecyclerViewAdapter = SearchPlaceRecyclerViewAdapter(
            onSearchItemClicked = { searchItem ->
                val locationWeatherForecastIntent = Intent(this, LocationWeatherForecastActivity::class.java)
                locationWeatherForecastIntent.putExtra(PLACE_ITEM_MODEL, searchItem.toPlaceItemModel())
                startActivityWithTransition(locationWeatherForecastIntent)
            }
        )
        searchPlaceRecyclerView?.adapter = searchPlaceRecyclerViewAdapter
        toolbarTextView?.text = getString(R.string.search_place_toolbar_text)

        searchEditText.afterTextChangedDebounce(DELAY_MILLIS, { text ->
            searchPlaceRecyclerView.visibility = if (text.isEmpty()) View.GONE else View.VISIBLE
            presenter?.getSuggestions(text)
        }, {})

        backButtonImageView.setOnClickListener {
            closeSoftKeyboard()
            finish()
        }
    }

    override fun showSuggestions(placesList: List<SearchItem>) {
        searchPlaceRecyclerView.visibility = View.VISIBLE
        searchPlaceRecyclerViewAdapter.setList(placesList)
    }

    override fun showEmpty() {
       showDialog(titleText = "Λάθος αναζήτηση", messageText = "Δεν βρέθηκαν αποτελέσματα για την περιοχή που δώσατε, Παρακαλώ επιλέξτε κάποια άλλη.",
       rightButtonText = "Ok",
       rightButtonListener = {
           it.dismiss()
       })

        searchPlaceRecyclerView.visibility = View.GONE
    }


}