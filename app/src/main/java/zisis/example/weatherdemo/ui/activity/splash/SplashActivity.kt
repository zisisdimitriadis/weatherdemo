package zisis.example.weatherdemo.ui.activity.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_splash.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.ui.activity.base.BaseActivity
import zisis.example.weatherdemo.ui.activity.home.HomeActivity

class SplashActivity : BaseActivity() {

    companion object {
        const val SPLASH_DELAY_IN_MILLIS = 3000L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initLayout()
    }

    private fun initLayout() {
        val animationInsideTopRight =
            AnimationUtils.loadAnimation(this, R.anim.anim_inside_top_right)
        sunImageView.startAnimation(animationInsideTopRight)

        val animInsideBottomToTop: Animation =
            AnimationUtils.loadAnimation(this, R.anim.anim_inside_bottom_to_top)
        rainImageView.startAnimation(animInsideBottomToTop)

        Handler().postDelayed({
            val homeIntent = Intent(this, HomeActivity::class.java)
            startActivityWithTransition(homeIntent)
            finish()
        }, SPLASH_DELAY_IN_MILLIS)
    }
}
