package zisis.example.weatherdemo.ui.adapter.forecast.hourly

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_forecast_item.*
import kotlinx.android.synthetic.main.row_hourly_item.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.common.extensions.loadUrl
import zisis.example.weatherdemo.models.parsers.response.forecast.Hourly
import zisis.example.weatherdemo.ui.adapter.forecast.ForecastRecyclerViewAdapter

class HourlyForecastRecyclerViewAdapter :
    RecyclerView.Adapter<HourlyForecastRecyclerViewAdapter.WeatherForecastItemViewHolder>() {

    private var hourlyList = mutableListOf<Hourly>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WeatherForecastItemViewHolder {
        return WeatherForecastItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_hourly_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return hourlyList.size
    }

    override fun onBindViewHolder(holder: WeatherForecastItemViewHolder, position: Int) {
        val hourly = hourlyList[position]
        holder.bind(hourly)

    }

    fun setHourlyList(hourlyList: List<Hourly>) {
        this.hourlyList.clear()
        this.hourlyList = hourlyList.toMutableList()
        notifyDataSetChanged()
    }

    class WeatherForecastItemViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        fun bind(hourly: Hourly) {
            hourlyTextView?.text = getTimeIn24Format(hourly.time ?: "")
            hourlyForecastImageView?.loadUrl(
                hourly.weatherIconUrlList?.get(0)?.value?.replace(
                    "http",
                    "https"
                )
            )
            hourlyTempTextView?.text = hourly.tempC
        }
    }
}

fun getTimeIn24Format(time: String): String {
    return when (time) {
        "0" -> "00:00"
        "300" -> "03:00"
        "600" -> "06:00"
        "900" -> "09:00"
        "1200" -> "12:00"
        "1500" -> "15:00"
        "1800" -> "18:00"
        "2100" -> "21:00"
        else -> ""
    }
}
