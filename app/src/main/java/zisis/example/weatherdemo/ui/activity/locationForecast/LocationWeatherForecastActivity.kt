package zisis.example.weatherdemo.ui.activity.locationForecast

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_location_weather_forecast.*
import kotlinx.android.synthetic.main.layout_current_weather_view.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.common.application.WeatherDemoApplication
import zisis.example.weatherdemo.common.extensions.loadUrl
import zisis.example.weatherdemo.models.parsers.response.forecast.ForecastCurrentCondition
import zisis.example.weatherdemo.models.parsers.response.forecast.Weather
import zisis.example.weatherdemo.models.viewModel.place.PlaceItemModel
import zisis.example.weatherdemo.mvp.interactor.locationWeatherForecast.LocationWeatherForecastInteractorImpl
import zisis.example.weatherdemo.mvp.presenter.locationWeatherForecast.LocationWeatherForecastPresenter
import zisis.example.weatherdemo.mvp.presenter.locationWeatherForecast.LocationWeatherForecastPresenterImpl
import zisis.example.weatherdemo.mvp.view.locationWeatherForecast.LocationWeatherForecastView
import zisis.example.weatherdemo.ui.activity.base.BaseMVPActivity
import zisis.example.weatherdemo.ui.activity.details.WeatherForecastDetailsActivity
import zisis.example.weatherdemo.ui.adapter.forecast.ForecastRecyclerViewAdapter


class LocationWeatherForecastActivity : BaseMVPActivity<LocationWeatherForecastPresenter>(),
    LocationWeatherForecastView {

    private lateinit var placeItemModel: PlaceItemModel
    private lateinit var forecastRecyclerViewAdapter: ForecastRecyclerViewAdapter

    companion object {
        const val PLACE_ITEM_MODEL = "PlaceItemModel"
        const val WEATHER_INFO = "WeatherInfo"
        const val POSITION = "position"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_weather_forecast)
        getPassData()
        presenter = LocationWeatherForecastPresenterImpl(
            this,
            LocationWeatherForecastInteractorImpl(WeatherDemoApplication.get().networkProvider, WeatherDemoApplication.get().weatherDemoDatabase)
        )
        initLayout()
        presenter?.showFavoriteUIIfNeeded(placeItemModel)
    }

    override fun showEmptyCurrentWeather() {

    }

    @SuppressLint("SetTextI18n")
    override fun showCurrentWeather(forecastCurrentCondition: ForecastCurrentCondition?) {
        currentWeatherImageView?.loadUrl(forecastCurrentCondition?.weatherIcon?.get(0)?.value?.replace("http", "https") ?: "")
        currentWeatherTempTextView?.text = forecastCurrentCondition?.tempC + "\u2103"
        currentMainWeatherTextView?.text = forecastCurrentCondition?.weatherDescription?.get(0)?.value ?: "-"
        currentWeatherHumidityTextView?.text = (forecastCurrentCondition?.humidity ?: "-") + "\u00B0"
    }

    override fun showEmptyForecast() {
        forecastRecyclerView?.visibility = View.GONE
        emptyForecastTextView?.visibility = View.VISIBLE
    }

    override fun showForecast(weatherModelList: List<Weather>) {
        forecastRecyclerView?.visibility = View.VISIBLE
        forecastRecyclerViewAdapter.setWeatherList(weatherModelList)
    }

    override fun updateFavoriteUI(isFavorite: Boolean) {
       favoriteImageView.setImageResource(if (isFavorite) R.drawable.ic_icon_favorite else R.drawable.ic_icon_unfavorite)
    }

    private fun getPassData() {
        placeItemModel = intent?.extras?.getParcelable(PLACE_ITEM_MODEL) ?: PlaceItemModel()
    }

    private fun initLayout() {
        forecastRecyclerView?.layoutManager = LinearLayoutManager(this,  LinearLayoutManager.HORIZONTAL, false)
        forecastRecyclerViewAdapter = ForecastRecyclerViewAdapter(
            onWeatherForecastClicked = { weather, position ->
            val detailsIntent = Intent(this, WeatherForecastDetailsActivity::class.java)
                detailsIntent.putExtra(WEATHER_INFO, weather)
                detailsIntent.putExtra(POSITION, position)
//                val cardViewPair = Pair.create<View, String>(mainView, mainView.transitionName)
//                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, cardViewPair)
//                startActivity(detailsIntent, options.toBundle())

                // alternative
                startActivityModal(detailsIntent)
            }
        )
        forecastRecyclerView?.adapter = forecastRecyclerViewAdapter
        toolbarTextView?.text = placeItemModel.getDisplayTitle()
        favoriteImageView.visibility = View.VISIBLE
        presenter?.getWeatherForecast(placeItemModel.getLatLngForQuery())

        favoriteImageView?.setOnClickListener {
            presenter?.setPlaceToFavorites(placeItemModel)
        }

        backButtonImageView.setOnClickListener {
            finish()
            overridePendingTransition(0, R.anim.animation_slide_out_right)
        }

    }
}