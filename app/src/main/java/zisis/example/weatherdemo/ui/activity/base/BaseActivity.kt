package zisis.example.weatherdemo.ui.activity.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.afollestad.materialdialogs.DialogCallback
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.activity_root.*
import kotlinx.android.synthetic.main.layout_no_internet_view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import zisis.example.weatherdemo.R
import zisis.example.weatherdemo.common.application.WeatherDemoApplication
import zisis.example.weatherdemo.models.eventBus.ConnectivityEvent

open class BaseActivity : AppCompatActivity() {

    private var materialDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(R.layout.activity_root)
        val layout = layoutInflater.inflate(layoutResID, containerView, false)
        containerView?.addView(layout)
    }

    override fun onDestroy() {
        super.onDestroy()
        materialDialog?.dismiss()
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
        (application as WeatherDemoApplication).registerConnectivityReceiverChange()
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onConnectivityChange(connectivityEvent: ConnectivityEvent) {
        if (isFinishing) {
            return
        }
        noInternetTextView?.visibility = if (connectivityEvent.hasNetwork) View.GONE else View.VISIBLE
        EventBus.getDefault().removeStickyEvent(connectivityEvent)
    }

    fun startActivityWithTransition(intent: Intent) {
        startActivity(intent)
        overridePendingTransition(R.anim.animation_slide_in_right, R.anim.animation_zoom_out)
    }

    fun startActivityModal(intent: Intent) {
        startActivity(intent)
        overridePendingTransition(R.anim.animation_slide_up, R.anim.animation_zoom_out)
    }

    protected fun initToolbar(toolbar: Toolbar?, isHomeUpEnabled: Boolean, titleToolbar: String?) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(isHomeUpEnabled)
        supportActionBar?.setDisplayShowHomeEnabled(isHomeUpEnabled)
        supportActionBar?.title = titleToolbar
    }

    fun showDialog(
        titleText: String = "",
        messageText: String = "",
        leftButtonText: String = "",
        rightButtonText: String = "",
        leftButtonListener: DialogCallback? = null,
        rightButtonListener: DialogCallback? = null
    ) {

        materialDialog?.dismiss()
        materialDialog = MaterialDialog(this).show {
            title(text = titleText)
            message(text = messageText)
            positiveButton(text = rightButtonText) {
                rightButtonListener?.invoke(this)
            }
            negativeButton(text = leftButtonText) {
                leftButtonListener?.invoke(this)
            }

        }
    }

}