{
    "data": {
        "request": [
            {
                "type": "LatLon",
                "query": "Lat 37.98 and Lon 23.72"
            }
        ],
        "current_condition": [
            {
                "observation_time": "04:30 PM",
                "temp_C": "18",
                "temp_F": "64",
                "weatherCode": "116",
                "weatherIconUrl": [
                    {
                        "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                    }
                ],
                "weatherDesc": [
                    {
                        "value": "Partly cloudy"
                    }
                ],
                "windspeedMiles": "0",
                "windspeedKmph": "0",
                "winddirDegree": "195",
                "winddir16Point": "SSW",
                "precipMM": "2.0",
                "precipInches": "0.1",
                "humidity": "68",
                "visibility": "10",
                "visibilityMiles": "6",
                "pressure": "1016",
                "pressureInches": "30",
                "cloudcover": "50",
                "FeelsLikeC": "18",
                "FeelsLikeF": "64",
                "uvIndex": 7
            }
        ],
        "weather": [
            {
                "date": "2020-05-28",
                "astronomy": [
                    {
                        "sunrise": "06:06 AM",
                        "sunset": "08:39 PM",
                        "moonrise": "11:06 AM",
                        "moonset": "01:00 AM",
                        "moon_phase": "First Quarter",
                        "moon_illumination": "39"
                    }
                ],
                "maxtempC": "21",
                "maxtempF": "69",
                "mintempC": "13",
                "mintempF": "55",
                "avgtempC": "18",
                "avgtempF": "64",
                "totalSnow_cm": "0.0",
                "sunHour": "8.3",
                "uvIndex": "7",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "15",
                        "tempF": "58",
                        "windspeedMiles": "5",
                        "windspeedKmph": "8",
                        "winddirDegree": "340",
                        "winddir16Point": "NNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "67",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1020",
                        "pressureInches": "31",
                        "cloudcover": "8",
                        "HeatIndexC": "15",
                        "HeatIndexF": "58",
                        "DewPointC": "9",
                        "DewPointF": "47",
                        "WindChillC": "14",
                        "WindChillF": "58",
                        "WindGustMiles": "7",
                        "WindGustKmph": "12",
                        "FeelsLikeC": "14",
                        "FeelsLikeF": "58",
                        "chanceofrain": "0",
                        "chanceofremdry": "90",
                        "chanceofwindy": "0",
                        "chanceofovercast": "39",
                        "chanceofsunshine": "81",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "300",
                        "tempC": "13",
                        "tempF": "56",
                        "windspeedMiles": "4",
                        "windspeedKmph": "7",
                        "winddirDegree": "339",
                        "winddir16Point": "NNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "66",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1019",
                        "pressureInches": "31",
                        "cloudcover": "12",
                        "HeatIndexC": "13",
                        "HeatIndexF": "56",
                        "DewPointC": "7",
                        "DewPointF": "45",
                        "WindChillC": "13",
                        "WindChillF": "56",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "13",
                        "FeelsLikeF": "56",
                        "chanceofrain": "0",
                        "chanceofremdry": "87",
                        "chanceofwindy": "0",
                        "chanceofovercast": "41",
                        "chanceofsunshine": "76",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "600",
                        "tempC": "14",
                        "tempF": "58",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "332",
                        "winddir16Point": "NNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "61",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1019",
                        "pressureInches": "31",
                        "cloudcover": "17",
                        "HeatIndexC": "14",
                        "HeatIndexF": "58",
                        "DewPointC": "7",
                        "DewPointF": "44",
                        "WindChillC": "14",
                        "WindChillF": "58",
                        "WindGustMiles": "7",
                        "WindGustKmph": "11",
                        "FeelsLikeC": "14",
                        "FeelsLikeF": "58",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "35",
                        "chanceofsunshine": "74",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "900",
                        "tempC": "19",
                        "tempF": "66",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "299",
                        "winddir16Point": "WNW",
                        "weatherCode": "176",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0009_light_rain_showers.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Patchy rain possible"
                            }
                        ],
                        "precipMM": "0.1",
                        "precipInches": "0.0",
                        "humidity": "50",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1019",
                        "pressureInches": "31",
                        "cloudcover": "38",
                        "HeatIndexC": "19",
                        "HeatIndexF": "66",
                        "DewPointC": "8",
                        "DewPointF": "47",
                        "WindChillC": "19",
                        "WindChillF": "66",
                        "WindGustMiles": "4",
                        "WindGustKmph": "6",
                        "FeelsLikeC": "19",
                        "FeelsLikeF": "66",
                        "chanceofrain": "33",
                        "chanceofremdry": "63",
                        "chanceofwindy": "0",
                        "chanceofovercast": "47",
                        "chanceofsunshine": "53",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "7"
                    },
                    {
                        "time": "1200",
                        "tempC": "20",
                        "tempF": "69",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "242",
                        "winddir16Point": "WSW",
                        "weatherCode": "356",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0010_heavy_rain_showers.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Moderate or heavy rain shower"
                            }
                        ],
                        "precipMM": "1.2",
                        "precipInches": "0.0",
                        "humidity": "45",
                        "visibility": "8",
                        "visibilityMiles": "5",
                        "pressure": "1019",
                        "pressureInches": "31",
                        "cloudcover": "76",
                        "HeatIndexC": "20",
                        "HeatIndexF": "69",
                        "DewPointC": "8",
                        "DewPointF": "47",
                        "WindChillC": "20",
                        "WindChillF": "69",
                        "WindGustMiles": "6",
                        "WindGustKmph": "9",
                        "FeelsLikeC": "20",
                        "FeelsLikeF": "69",
                        "chanceofrain": "88",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "82",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "7"
                    },
                    {
                        "time": "1500",
                        "tempC": "20",
                        "tempF": "69",
                        "windspeedMiles": "7",
                        "windspeedKmph": "11",
                        "winddirDegree": "192",
                        "winddir16Point": "SSW",
                        "weatherCode": "176",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0009_light_rain_showers.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Patchy rain possible"
                            }
                        ],
                        "precipMM": "2.6",
                        "precipInches": "0.1",
                        "humidity": "47",
                        "visibility": "8",
                        "visibilityMiles": "4",
                        "pressure": "1017",
                        "pressureInches": "31",
                        "cloudcover": "83",
                        "HeatIndexC": "20",
                        "HeatIndexF": "69",
                        "DewPointC": "9",
                        "DewPointF": "48",
                        "WindChillC": "20",
                        "WindChillF": "69",
                        "WindGustMiles": "10",
                        "WindGustKmph": "17",
                        "FeelsLikeC": "20",
                        "FeelsLikeF": "69",
                        "chanceofrain": "69",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "84",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "7"
                    },
                    {
                        "time": "1800",
                        "tempC": "20",
                        "tempF": "67",
                        "windspeedMiles": "8",
                        "windspeedKmph": "13",
                        "winddirDegree": "195",
                        "winddir16Point": "SSW",
                        "weatherCode": "176",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0009_light_rain_showers.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Patchy rain possible"
                            }
                        ],
                        "precipMM": "1.5",
                        "precipInches": "0.1",
                        "humidity": "52",
                        "visibility": "9",
                        "visibilityMiles": "5",
                        "pressure": "1017",
                        "pressureInches": "30",
                        "cloudcover": "88",
                        "HeatIndexC": "20",
                        "HeatIndexF": "67",
                        "DewPointC": "9",
                        "DewPointF": "49",
                        "WindChillC": "20",
                        "WindChillF": "67",
                        "WindGustMiles": "11",
                        "WindGustKmph": "18",
                        "FeelsLikeC": "20",
                        "FeelsLikeF": "67",
                        "chanceofrain": "76",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "84",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "7"
                    },
                    {
                        "time": "2100",
                        "tempC": "18",
                        "tempF": "65",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "214",
                        "winddir16Point": "SW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "1.0",
                        "precipInches": "0.0",
                        "humidity": "60",
                        "visibility": "9",
                        "visibilityMiles": "5",
                        "pressure": "1016",
                        "pressureInches": "30",
                        "cloudcover": "59",
                        "HeatIndexC": "18",
                        "HeatIndexF": "65",
                        "DewPointC": "10",
                        "DewPointF": "51",
                        "WindChillC": "18",
                        "WindChillF": "65",
                        "WindGustMiles": "5",
                        "WindGustKmph": "8",
                        "FeelsLikeC": "18",
                        "FeelsLikeF": "65",
                        "chanceofrain": "49",
                        "chanceofremdry": "31",
                        "chanceofwindy": "0",
                        "chanceofovercast": "74",
                        "chanceofsunshine": "27",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    }
                ]
            },
            {
                "date": "2020-05-29",
                "astronomy": [
                    {
                        "sunrise": "06:05 AM",
                        "sunset": "08:40 PM",
                        "moonrise": "12:13 PM",
                        "moonset": "01:41 AM",
                        "moon_phase": "First Quarter",
                        "moon_illumination": "47"
                    }
                ],
                "maxtempC": "22",
                "maxtempF": "72",
                "mintempC": "14",
                "mintempF": "58",
                "avgtempC": "19",
                "avgtempF": "66",
                "totalSnow_cm": "0.0",
                "sunHour": "10.5",
                "uvIndex": "8",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "16",
                        "tempF": "62",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "276",
                        "winddir16Point": "W",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "66",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1016",
                        "pressureInches": "30",
                        "cloudcover": "8",
                        "HeatIndexC": "16",
                        "HeatIndexF": "62",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "16",
                        "WindChillF": "62",
                        "WindGustMiles": "5",
                        "WindGustKmph": "8",
                        "FeelsLikeC": "16",
                        "FeelsLikeF": "62",
                        "chanceofrain": "0",
                        "chanceofremdry": "91",
                        "chanceofwindy": "0",
                        "chanceofovercast": "42",
                        "chanceofsunshine": "81",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "300",
                        "tempC": "15",
                        "tempF": "59",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "300",
                        "winddir16Point": "WNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "70",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1016",
                        "pressureInches": "30",
                        "cloudcover": "14",
                        "HeatIndexC": "15",
                        "HeatIndexF": "59",
                        "DewPointC": "10",
                        "DewPointF": "49",
                        "WindChillC": "15",
                        "WindChillF": "59",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "15",
                        "FeelsLikeF": "59",
                        "chanceofrain": "0",
                        "chanceofremdry": "86",
                        "chanceofwindy": "0",
                        "chanceofovercast": "37",
                        "chanceofsunshine": "79",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "600",
                        "tempC": "16",
                        "tempF": "61",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "323",
                        "winddir16Point": "NW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "67",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1016",
                        "pressureInches": "30",
                        "cloudcover": "16",
                        "HeatIndexC": "16",
                        "HeatIndexF": "61",
                        "DewPointC": "10",
                        "DewPointF": "49",
                        "WindChillC": "16",
                        "WindChillF": "61",
                        "WindGustMiles": "6",
                        "WindGustKmph": "10",
                        "FeelsLikeC": "16",
                        "FeelsLikeF": "61",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "38",
                        "chanceofsunshine": "79",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "900",
                        "tempC": "20",
                        "tempF": "68",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "316",
                        "winddir16Point": "NW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "50",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1017",
                        "pressureInches": "30",
                        "cloudcover": "17",
                        "HeatIndexC": "21",
                        "HeatIndexF": "70",
                        "DewPointC": "9",
                        "DewPointF": "49",
                        "WindChillC": "20",
                        "WindChillF": "68",
                        "WindGustMiles": "4",
                        "WindGustKmph": "7",
                        "FeelsLikeC": "20",
                        "FeelsLikeF": "68",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "44",
                        "chanceofsunshine": "75",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "1200",
                        "tempC": "22",
                        "tempF": "72",
                        "windspeedMiles": "6",
                        "windspeedKmph": "9",
                        "winddirDegree": "243",
                        "winddir16Point": "WSW",
                        "weatherCode": "176",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0009_light_rain_showers.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Patchy rain possible"
                            }
                        ],
                        "precipMM": "0.1",
                        "precipInches": "0.0",
                        "humidity": "41",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1017",
                        "pressureInches": "31",
                        "cloudcover": "39",
                        "HeatIndexC": "24",
                        "HeatIndexF": "75",
                        "DewPointC": "8",
                        "DewPointF": "47",
                        "WindChillC": "22",
                        "WindChillF": "72",
                        "WindGustMiles": "6",
                        "WindGustKmph": "10",
                        "FeelsLikeC": "24",
                        "FeelsLikeF": "75",
                        "chanceofrain": "28",
                        "chanceofremdry": "57",
                        "chanceofwindy": "0",
                        "chanceofovercast": "59",
                        "chanceofsunshine": "55",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "1500",
                        "tempC": "22",
                        "tempF": "72",
                        "windspeedMiles": "11",
                        "windspeedKmph": "18",
                        "winddirDegree": "204",
                        "winddir16Point": "SSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.1",
                        "precipInches": "0.0",
                        "humidity": "42",
                        "visibility": "9",
                        "visibilityMiles": "5",
                        "pressure": "1016",
                        "pressureInches": "30",
                        "cloudcover": "66",
                        "HeatIndexC": "24",
                        "HeatIndexF": "76",
                        "DewPointC": "9",
                        "DewPointF": "47",
                        "WindChillC": "22",
                        "WindChillF": "72",
                        "WindGustMiles": "13",
                        "WindGustKmph": "21",
                        "FeelsLikeC": "24",
                        "FeelsLikeF": "76",
                        "chanceofrain": "57",
                        "chanceofremdry": "31",
                        "chanceofwindy": "0",
                        "chanceofovercast": "67",
                        "chanceofsunshine": "24",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "1800",
                        "tempC": "21",
                        "tempF": "69",
                        "windspeedMiles": "11",
                        "windspeedKmph": "18",
                        "winddirDegree": "212",
                        "winddir16Point": "SSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "52",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1015",
                        "pressureInches": "30",
                        "cloudcover": "28",
                        "HeatIndexC": "23",
                        "HeatIndexF": "73",
                        "DewPointC": "11",
                        "DewPointF": "51",
                        "WindChillC": "21",
                        "WindChillF": "69",
                        "WindGustMiles": "13",
                        "WindGustKmph": "21",
                        "FeelsLikeC": "21",
                        "FeelsLikeF": "69",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "36",
                        "chanceofsunshine": "72",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "2100",
                        "tempC": "19",
                        "tempF": "65",
                        "windspeedMiles": "5",
                        "windspeedKmph": "9",
                        "winddirDegree": "226",
                        "winddir16Point": "SW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "60",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1015",
                        "pressureInches": "30",
                        "cloudcover": "14",
                        "HeatIndexC": "19",
                        "HeatIndexF": "65",
                        "DewPointC": "11",
                        "DewPointF": "51",
                        "WindChillC": "19",
                        "WindChillF": "65",
                        "WindGustMiles": "7",
                        "WindGustKmph": "11",
                        "FeelsLikeC": "19",
                        "FeelsLikeF": "65",
                        "chanceofrain": "0",
                        "chanceofremdry": "82",
                        "chanceofwindy": "0",
                        "chanceofovercast": "36",
                        "chanceofsunshine": "74",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    }
                ]
            },
            {
                "date": "2020-05-30",
                "astronomy": [
                    {
                        "sunrise": "06:05 AM",
                        "sunset": "08:41 PM",
                        "moonrise": "01:23 PM",
                        "moonset": "02:17 AM",
                        "moon_phase": "First Quarter",
                        "moon_illumination": "54"
                    }
                ],
                "maxtempC": "23",
                "maxtempF": "73",
                "mintempC": "15",
                "mintempF": "59",
                "avgtempC": "20",
                "avgtempF": "68",
                "totalSnow_cm": "0.0",
                "sunHour": "11.6",
                "uvIndex": "9",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "17",
                        "tempF": "63",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "255",
                        "winddir16Point": "WSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "62",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1015",
                        "pressureInches": "30",
                        "cloudcover": "10",
                        "HeatIndexC": "17",
                        "HeatIndexF": "63",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "17",
                        "WindChillF": "63",
                        "WindGustMiles": "5",
                        "WindGustKmph": "8",
                        "FeelsLikeC": "17",
                        "FeelsLikeF": "63",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "40",
                        "chanceofsunshine": "75",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "300",
                        "tempC": "16",
                        "tempF": "61",
                        "windspeedMiles": "4",
                        "windspeedKmph": "7",
                        "winddirDegree": "286",
                        "winddir16Point": "WNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "63",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1015",
                        "pressureInches": "30",
                        "cloudcover": "4",
                        "HeatIndexC": "16",
                        "HeatIndexF": "61",
                        "DewPointC": "9",
                        "DewPointF": "49",
                        "WindChillC": "16",
                        "WindChillF": "61",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "16",
                        "FeelsLikeF": "61",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "44",
                        "chanceofsunshine": "75",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "600",
                        "tempC": "17",
                        "tempF": "63",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "302",
                        "winddir16Point": "WNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "61",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1015",
                        "pressureInches": "30",
                        "cloudcover": "5",
                        "HeatIndexC": "17",
                        "HeatIndexF": "63",
                        "DewPointC": "9",
                        "DewPointF": "49",
                        "WindChillC": "17",
                        "WindChillF": "63",
                        "WindGustMiles": "7",
                        "WindGustKmph": "11",
                        "FeelsLikeC": "17",
                        "FeelsLikeF": "63",
                        "chanceofrain": "0",
                        "chanceofremdry": "84",
                        "chanceofwindy": "0",
                        "chanceofovercast": "41",
                        "chanceofsunshine": "80",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "900",
                        "tempC": "21",
                        "tempF": "70",
                        "windspeedMiles": "3",
                        "windspeedKmph": "4",
                        "winddirDegree": "273",
                        "winddir16Point": "W",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "48",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1016",
                        "pressureInches": "30",
                        "cloudcover": "7",
                        "HeatIndexC": "22",
                        "HeatIndexF": "71",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "21",
                        "WindChillF": "70",
                        "WindGustMiles": "3",
                        "WindGustKmph": "5",
                        "FeelsLikeC": "21",
                        "FeelsLikeF": "70",
                        "chanceofrain": "0",
                        "chanceofremdry": "85",
                        "chanceofwindy": "0",
                        "chanceofovercast": "38",
                        "chanceofsunshine": "81",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "9"
                    },
                    {
                        "time": "1200",
                        "tempC": "23",
                        "tempF": "73",
                        "windspeedMiles": "7",
                        "windspeedKmph": "12",
                        "winddirDegree": "208",
                        "winddir16Point": "SSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "41",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1016",
                        "pressureInches": "30",
                        "cloudcover": "8",
                        "HeatIndexC": "24",
                        "HeatIndexF": "76",
                        "DewPointC": "9",
                        "DewPointF": "48",
                        "WindChillC": "23",
                        "WindChillF": "73",
                        "WindGustMiles": "9",
                        "WindGustKmph": "14",
                        "FeelsLikeC": "24",
                        "FeelsLikeF": "76",
                        "chanceofrain": "0",
                        "chanceofremdry": "85",
                        "chanceofwindy": "0",
                        "chanceofovercast": "34",
                        "chanceofsunshine": "74",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "3",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "9"
                    },
                    {
                        "time": "1500",
                        "tempC": "23",
                        "tempF": "73",
                        "windspeedMiles": "10",
                        "windspeedKmph": "17",
                        "winddirDegree": "199",
                        "winddir16Point": "SSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "41",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1015",
                        "pressureInches": "30",
                        "cloudcover": "6",
                        "HeatIndexC": "24",
                        "HeatIndexF": "76",
                        "DewPointC": "9",
                        "DewPointF": "47",
                        "WindChillC": "23",
                        "WindChillF": "73",
                        "WindGustMiles": "12",
                        "WindGustKmph": "19",
                        "FeelsLikeC": "24",
                        "FeelsLikeF": "76",
                        "chanceofrain": "0",
                        "chanceofremdry": "91",
                        "chanceofwindy": "0",
                        "chanceofovercast": "41",
                        "chanceofsunshine": "81",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "7",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "9"
                    },
                    {
                        "time": "1800",
                        "tempC": "21",
                        "tempF": "70",
                        "windspeedMiles": "8",
                        "windspeedKmph": "13",
                        "winddirDegree": "186",
                        "winddir16Point": "S",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "51",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1014",
                        "pressureInches": "30",
                        "cloudcover": "7",
                        "HeatIndexC": "23",
                        "HeatIndexF": "73",
                        "DewPointC": "11",
                        "DewPointF": "51",
                        "WindChillC": "21",
                        "WindChillF": "70",
                        "WindGustMiles": "10",
                        "WindGustKmph": "15",
                        "FeelsLikeC": "21",
                        "FeelsLikeF": "70",
                        "chanceofrain": "0",
                        "chanceofremdry": "84",
                        "chanceofwindy": "0",
                        "chanceofovercast": "39",
                        "chanceofsunshine": "77",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "9"
                    },
                    {
                        "time": "2100",
                        "tempC": "19",
                        "tempF": "67",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "210",
                        "winddir16Point": "SSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "55",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1014",
                        "pressureInches": "30",
                        "cloudcover": "12",
                        "HeatIndexC": "19",
                        "HeatIndexF": "67",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "19",
                        "WindChillF": "67",
                        "WindGustMiles": "4",
                        "WindGustKmph": "6",
                        "FeelsLikeC": "19",
                        "FeelsLikeF": "67",
                        "chanceofrain": "0",
                        "chanceofremdry": "85",
                        "chanceofwindy": "0",
                        "chanceofovercast": "30",
                        "chanceofsunshine": "79",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    }
                ]
            },
            {
                "date": "2020-05-31",
                "astronomy": [
                    {
                        "sunrise": "06:05 AM",
                        "sunset": "08:42 PM",
                        "moonrise": "02:32 PM",
                        "moonset": "02:51 AM",
                        "moon_phase": "First Quarter",
                        "moon_illumination": "58"
                    }
                ],
                "maxtempC": "24",
                "maxtempF": "74",
                "mintempC": "15",
                "mintempF": "60",
                "avgtempC": "20",
                "avgtempF": "68",
                "totalSnow_cm": "0.0",
                "sunHour": "11.6",
                "uvIndex": "8",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "17",
                        "tempF": "63",
                        "windspeedMiles": "2",
                        "windspeedKmph": "4",
                        "winddirDegree": "282",
                        "winddir16Point": "WNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "61",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1014",
                        "pressureInches": "30",
                        "cloudcover": "14",
                        "HeatIndexC": "17",
                        "HeatIndexF": "63",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "17",
                        "WindChillF": "63",
                        "WindGustMiles": "4",
                        "WindGustKmph": "6",
                        "FeelsLikeC": "17",
                        "FeelsLikeF": "63",
                        "chanceofrain": "0",
                        "chanceofremdry": "90",
                        "chanceofwindy": "0",
                        "chanceofovercast": "35",
                        "chanceofsunshine": "80",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "300",
                        "tempC": "16",
                        "tempF": "61",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "295",
                        "winddir16Point": "WNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "67",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1013",
                        "pressureInches": "30",
                        "cloudcover": "15",
                        "HeatIndexC": "16",
                        "HeatIndexF": "61",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "16",
                        "WindChillF": "61",
                        "WindGustMiles": "6",
                        "WindGustKmph": "10",
                        "FeelsLikeC": "16",
                        "FeelsLikeF": "61",
                        "chanceofrain": "0",
                        "chanceofremdry": "87",
                        "chanceofwindy": "0",
                        "chanceofovercast": "46",
                        "chanceofsunshine": "74",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "600",
                        "tempC": "17",
                        "tempF": "63",
                        "windspeedMiles": "5",
                        "windspeedKmph": "8",
                        "winddirDegree": "285",
                        "winddir16Point": "WNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "65",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1014",
                        "pressureInches": "30",
                        "cloudcover": "9",
                        "HeatIndexC": "17",
                        "HeatIndexF": "63",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "17",
                        "WindChillF": "63",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "17",
                        "FeelsLikeF": "63",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "48",
                        "chanceofsunshine": "76",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "900",
                        "tempC": "21",
                        "tempF": "70",
                        "windspeedMiles": "8",
                        "windspeedKmph": "13",
                        "winddirDegree": "262",
                        "winddir16Point": "W",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "49",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1014",
                        "pressureInches": "30",
                        "cloudcover": "6",
                        "HeatIndexC": "22",
                        "HeatIndexF": "71",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "21",
                        "WindChillF": "70",
                        "WindGustMiles": "9",
                        "WindGustKmph": "15",
                        "FeelsLikeC": "21",
                        "FeelsLikeF": "70",
                        "chanceofrain": "0",
                        "chanceofremdry": "85",
                        "chanceofwindy": "0",
                        "chanceofovercast": "45",
                        "chanceofsunshine": "73",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "1200",
                        "tempC": "23",
                        "tempF": "73",
                        "windspeedMiles": "12",
                        "windspeedKmph": "20",
                        "winddirDegree": "242",
                        "winddir16Point": "WSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "38",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1014",
                        "pressureInches": "30",
                        "cloudcover": "10",
                        "HeatIndexC": "24",
                        "HeatIndexF": "76",
                        "DewPointC": "8",
                        "DewPointF": "47",
                        "WindChillC": "23",
                        "WindChillF": "73",
                        "WindGustMiles": "14",
                        "WindGustKmph": "23",
                        "FeelsLikeC": "24",
                        "FeelsLikeF": "76",
                        "chanceofrain": "0",
                        "chanceofremdry": "83",
                        "chanceofwindy": "0",
                        "chanceofovercast": "42",
                        "chanceofsunshine": "77",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "3",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "1500",
                        "tempC": "23",
                        "tempF": "73",
                        "windspeedMiles": "12",
                        "windspeedKmph": "19",
                        "winddirDegree": "223",
                        "winddir16Point": "SW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "41",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1013",
                        "pressureInches": "30",
                        "cloudcover": "11",
                        "HeatIndexC": "24",
                        "HeatIndexF": "76",
                        "DewPointC": "9",
                        "DewPointF": "48",
                        "WindChillC": "23",
                        "WindChillF": "73",
                        "WindGustMiles": "14",
                        "WindGustKmph": "22",
                        "FeelsLikeC": "24",
                        "FeelsLikeF": "76",
                        "chanceofrain": "0",
                        "chanceofremdry": "88",
                        "chanceofwindy": "0",
                        "chanceofovercast": "43",
                        "chanceofsunshine": "73",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "7",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "1800",
                        "tempC": "21",
                        "tempF": "70",
                        "windspeedMiles": "7",
                        "windspeedKmph": "12",
                        "winddirDegree": "206",
                        "winddir16Point": "SSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "54",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1012",
                        "pressureInches": "30",
                        "cloudcover": "13",
                        "HeatIndexC": "23",
                        "HeatIndexF": "74",
                        "DewPointC": "12",
                        "DewPointF": "53",
                        "WindChillC": "21",
                        "WindChillF": "70",
                        "WindGustMiles": "9",
                        "WindGustKmph": "14",
                        "FeelsLikeC": "21",
                        "FeelsLikeF": "70",
                        "chanceofrain": "0",
                        "chanceofremdry": "91",
                        "chanceofwindy": "0",
                        "chanceofovercast": "40",
                        "chanceofsunshine": "78",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "2100",
                        "tempC": "20",
                        "tempF": "68",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "208",
                        "winddir16Point": "SSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "59",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1012",
                        "pressureInches": "30",
                        "cloudcover": "9",
                        "HeatIndexC": "20",
                        "HeatIndexF": "68",
                        "DewPointC": "12",
                        "DewPointF": "53",
                        "WindChillC": "20",
                        "WindChillF": "68",
                        "WindGustMiles": "3",
                        "WindGustKmph": "5",
                        "FeelsLikeC": "20",
                        "FeelsLikeF": "68",
                        "chanceofrain": "0",
                        "chanceofremdry": "91",
                        "chanceofwindy": "0",
                        "chanceofovercast": "38",
                        "chanceofsunshine": "86",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    }
                ]
            },
            {
                "date": "2020-06-01",
                "astronomy": [
                    {
                        "sunrise": "06:04 AM",
                        "sunset": "08:42 PM",
                        "moonrise": "03:43 PM",
                        "moonset": "03:22 AM",
                        "moon_phase": "Waxing Gibbous",
                        "moon_illumination": "71"
                    }
                ],
                "maxtempC": "24",
                "maxtempF": "75",
                "mintempC": "16",
                "mintempF": "62",
                "avgtempC": "21",
                "avgtempF": "70",
                "totalSnow_cm": "0.0",
                "sunHour": "11.6",
                "uvIndex": "8",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "18",
                        "tempF": "65",
                        "windspeedMiles": "2",
                        "windspeedKmph": "4",
                        "winddirDegree": "254",
                        "winddir16Point": "WSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "63",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1012",
                        "pressureInches": "30",
                        "cloudcover": "4",
                        "HeatIndexC": "18",
                        "HeatIndexF": "65",
                        "DewPointC": "11",
                        "DewPointF": "53",
                        "WindChillC": "18",
                        "WindChillF": "65",
                        "WindGustMiles": "3",
                        "WindGustKmph": "5",
                        "FeelsLikeC": "18",
                        "FeelsLikeF": "65",
                        "chanceofrain": "0",
                        "chanceofremdry": "87",
                        "chanceofwindy": "0",
                        "chanceofovercast": "47",
                        "chanceofsunshine": "85",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "300",
                        "tempC": "17",
                        "tempF": "63",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "298",
                        "winddir16Point": "WNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "66",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1011",
                        "pressureInches": "30",
                        "cloudcover": "3",
                        "HeatIndexC": "17",
                        "HeatIndexF": "63",
                        "DewPointC": "11",
                        "DewPointF": "52",
                        "WindChillC": "17",
                        "WindChillF": "63",
                        "WindGustMiles": "5",
                        "WindGustKmph": "9",
                        "FeelsLikeC": "17",
                        "FeelsLikeF": "63",
                        "chanceofrain": "0",
                        "chanceofremdry": "88",
                        "chanceofwindy": "0",
                        "chanceofovercast": "41",
                        "chanceofsunshine": "79",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "600",
                        "tempC": "18",
                        "tempF": "65",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "329",
                        "winddir16Point": "NNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "61",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1011",
                        "pressureInches": "30",
                        "cloudcover": "3",
                        "HeatIndexC": "19",
                        "HeatIndexF": "66",
                        "DewPointC": "10",
                        "DewPointF": "51",
                        "WindChillC": "18",
                        "WindChillF": "65",
                        "WindGustMiles": "6",
                        "WindGustKmph": "9",
                        "FeelsLikeC": "18",
                        "FeelsLikeF": "65",
                        "chanceofrain": "0",
                        "chanceofremdry": "93",
                        "chanceofwindy": "0",
                        "chanceofovercast": "35",
                        "chanceofsunshine": "80",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    },
                    {
                        "time": "900",
                        "tempC": "23",
                        "tempF": "73",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "300",
                        "winddir16Point": "WNW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "47",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1012",
                        "pressureInches": "30",
                        "cloudcover": "8",
                        "HeatIndexC": "25",
                        "HeatIndexF": "76",
                        "DewPointC": "11",
                        "DewPointF": "51",
                        "WindChillC": "23",
                        "WindChillF": "73",
                        "WindGustMiles": "3",
                        "WindGustKmph": "5",
                        "FeelsLikeC": "25",
                        "FeelsLikeF": "76",
                        "chanceofrain": "0",
                        "chanceofremdry": "92",
                        "chanceofwindy": "0",
                        "chanceofovercast": "35",
                        "chanceofsunshine": "85",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "5",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "1200",
                        "tempC": "24",
                        "tempF": "75",
                        "windspeedMiles": "5",
                        "windspeedKmph": "8",
                        "winddirDegree": "195",
                        "winddir16Point": "SSW",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "41",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1011",
                        "pressureInches": "30",
                        "cloudcover": "20",
                        "HeatIndexC": "25",
                        "HeatIndexF": "76",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "24",
                        "WindChillF": "75",
                        "WindGustMiles": "6",
                        "WindGustKmph": "9",
                        "FeelsLikeC": "25",
                        "FeelsLikeF": "76",
                        "chanceofrain": "0",
                        "chanceofremdry": "86",
                        "chanceofwindy": "0",
                        "chanceofovercast": "46",
                        "chanceofsunshine": "85",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "13",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "1500",
                        "tempC": "23",
                        "tempF": "74",
                        "windspeedMiles": "9",
                        "windspeedKmph": "14",
                        "winddirDegree": "186",
                        "winddir16Point": "S",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "43",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1010",
                        "pressureInches": "30",
                        "cloudcover": "23",
                        "HeatIndexC": "25",
                        "HeatIndexF": "76",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "23",
                        "WindChillF": "74",
                        "WindGustMiles": "10",
                        "WindGustKmph": "17",
                        "FeelsLikeC": "25",
                        "FeelsLikeF": "76",
                        "chanceofrain": "0",
                        "chanceofremdry": "83",
                        "chanceofwindy": "0",
                        "chanceofovercast": "45",
                        "chanceofsunshine": "80",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "10",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "1800",
                        "tempC": "23",
                        "tempF": "73",
                        "windspeedMiles": "7",
                        "windspeedKmph": "11",
                        "winddirDegree": "178",
                        "winddir16Point": "S",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "47",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1010",
                        "pressureInches": "30",
                        "cloudcover": "11",
                        "HeatIndexC": "25",
                        "HeatIndexF": "76",
                        "DewPointC": "11",
                        "DewPointF": "51",
                        "WindChillC": "23",
                        "WindChillF": "73",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "25",
                        "FeelsLikeF": "76",
                        "chanceofrain": "0",
                        "chanceofremdry": "83",
                        "chanceofwindy": "0",
                        "chanceofovercast": "42",
                        "chanceofsunshine": "79",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "7",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "8"
                    },
                    {
                        "time": "2100",
                        "tempC": "21",
                        "tempF": "69",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "179",
                        "winddir16Point": "S",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "precipInches": "0.0",
                        "humidity": "57",
                        "visibility": "10",
                        "visibilityMiles": "6",
                        "pressure": "1010",
                        "pressureInches": "30",
                        "cloudcover": "9",
                        "HeatIndexC": "23",
                        "HeatIndexF": "73",
                        "DewPointC": "12",
                        "DewPointF": "53",
                        "WindChillC": "21",
                        "WindChillF": "69",
                        "WindGustMiles": "4",
                        "WindGustKmph": "6",
                        "FeelsLikeC": "21",
                        "FeelsLikeF": "69",
                        "chanceofrain": "0",
                        "chanceofremdry": "81",
                        "chanceofwindy": "0",
                        "chanceofovercast": "41",
                        "chanceofsunshine": "83",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0",
                        "uvIndex": "1"
                    }
                ]
            }
        ],
        "ClimateAverages": [
            {
                "month": [
                    {
                        "index": "1",
                        "name": "January",
                        "avgMinTemp": "7.3",
                        "avgMinTemp_F": "45.2",
                        "absMaxTemp": "14.035581",
                        "absMaxTemp_F": "57.3",
                        "avgDailyRainfall": "1.50"
                    },
                    {
                        "index": "2",
                        "name": "February",
                        "avgMinTemp": "8.3",
                        "avgMinTemp_F": "47.0",
                        "absMaxTemp": "16.886034",
                        "absMaxTemp_F": "62.4",
                        "avgDailyRainfall": "1.43"
                    },
                    {
                        "index": "3",
                        "name": "March",
                        "avgMinTemp": "9.8",
                        "avgMinTemp_F": "49.6",
                        "absMaxTemp": "18.203432",
                        "absMaxTemp_F": "64.8",
                        "avgDailyRainfall": "1.11"
                    },
                    {
                        "index": "4",
                        "name": "April",
                        "avgMinTemp": "12.0",
                        "avgMinTemp_F": "53.7",
                        "absMaxTemp": "22.30627",
                        "absMaxTemp_F": "72.2",
                        "avgDailyRainfall": "1.07"
                    },
                    {
                        "index": "5",
                        "name": "May",
                        "avgMinTemp": "16.5",
                        "avgMinTemp_F": "61.8",
                        "absMaxTemp": "25.930613",
                        "absMaxTemp_F": "78.7",
                        "avgDailyRainfall": "0.58"
                    },
                    {
                        "index": "6",
                        "name": "June",
                        "avgMinTemp": "20.8",
                        "avgMinTemp_F": "69.5",
                        "absMaxTemp": "30.723333",
                        "absMaxTemp_F": "87.3",
                        "avgDailyRainfall": "0.48"
                    },
                    {
                        "index": "7",
                        "name": "July",
                        "avgMinTemp": "23.5",
                        "avgMinTemp_F": "74.2",
                        "absMaxTemp": "32.341934",
                        "absMaxTemp_F": "90.2",
                        "avgDailyRainfall": "0.16"
                    },
                    {
                        "index": "8",
                        "name": "August",
                        "avgMinTemp": "23.8",
                        "avgMinTemp_F": "74.8",
                        "absMaxTemp": "32.687096",
                        "absMaxTemp_F": "90.8",
                        "avgDailyRainfall": "0.08"
                    },
                    {
                        "index": "9",
                        "name": "September",
                        "avgMinTemp": "20.8",
                        "avgMinTemp_F": "69.4",
                        "absMaxTemp": "28.5607",
                        "absMaxTemp_F": "83.4",
                        "avgDailyRainfall": "0.65"
                    },
                    {
                        "index": "10",
                        "name": "October",
                        "avgMinTemp": "16.9",
                        "avgMinTemp_F": "62.3",
                        "absMaxTemp": "25.383871",
                        "absMaxTemp_F": "77.7",
                        "avgDailyRainfall": "1.19"
                    },
                    {
                        "index": "11",
                        "name": "November",
                        "avgMinTemp": "13.4",
                        "avgMinTemp_F": "56.1",
                        "absMaxTemp": "20.1078",
                        "absMaxTemp_F": "68.2",
                        "avgDailyRainfall": "1.35"
                    },
                    {
                        "index": "12",
                        "name": "December",
                        "avgMinTemp": "9.2",
                        "avgMinTemp_F": "48.6",
                        "absMaxTemp": "15.789806",
                        "absMaxTemp_F": "60.4",
                        "avgDailyRainfall": "1.72"
                    }
                ]
            }
        ]
    }
}